//
//  SNAppDelegate.m
//  CatalogViewer
//
//  Created by Шурик on 09.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//



#import "SNAppDelegate.h"


#import "SNPublicationsController.h"
#import "SNKeychainWrapper.h"
#import "SNAppearence.h"

#import "Toolbox.h"
#import "DMManager.h"
#import "DMSharedPublications+CoreDataClass.h"

NSString * const SNAppLastUpdateKey = @"SNAppLastUpdateKey";


@implementation SNAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

+ (SNAppDelegate *)sharedAppDelegate
{
    return (SNAppDelegate *) [UIApplication sharedApplication].delegate;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Check exitance if UUID
    [self checkUniqueDeviceID];
    
    
    // Register for Apple Push Notifications
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_1) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                                                                                             |UIRemoteNotificationTypeSound
                                                                                             |UIRemoteNotificationTypeAlert) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
        // Clear application badge when app launches
    }
    
    application.applicationIconBadgeNumber = 0;
    
//    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
//        [SNAppearence customizeAppearance];
//        
//        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
//            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
//        }
//        
//        SNFirstController *firstController = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNFirstController class])];
//        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:firstController];
//        self.window.rootViewController = navController;
//        
//        
//        
//    }
    
    [SNAppearence customizeAppearance];
    
    // Stop the TimeOutCounter
    // to prevent App from going to sleep
    [[UIApplication sharedApplication] setIdleTimerDisabled: YES];
    
    // create default shared publications object
    [DMSharedPublications sharedInContext:nil];
    
    
    // Update new fields
    NSBatchUpdateRequest *batchRequest = [NSBatchUpdateRequest batchUpdateRequestWithEntityName:NSStringFromClass([DMPublication class])];
    batchRequest.predicate =[NSPredicate predicateWithFormat:@"unread = nil"];
    batchRequest.propertiesToUpdate = @{ @"unread" : @(YES) };
    
    batchRequest.resultType = NSUpdatedObjectsCountResultType;
    NSManagedObjectContext *context = [[DMManager sharedManager] defaultContext];
    NSError *error = nil;
    [context executeRequest:batchRequest error:&error];
    if (error) {
        NSLog(@"%s Error during fetching butch update: %@", __FUNCTION__, error);
    }

    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *) managedObjectContext {
    
    return [[DMManager sharedManager] defaultContext];
    
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    
    return _managedObjectContext;
}



- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"catalogServiceCoreData" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
    
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    NSURL *storeUrl = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"catalogServiceCoreData.sqlite"];
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error]) {
        NSLog(@"ERROR DURING CREATION OF COREDATA PERSISTENT STORE!");
    }
    
    return _persistentStoreCoordinator;
}

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}


- (void)checkUniqueDeviceID {
    // Check for Unique-Identifier
    // Get the stored data before the view loads
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.uuid = [defaults objectForKey:@"uuid"];
    
    if([self.uuid length] > 0) {
        // UUID existing
        NSLog(@"%s: application: didFinishLaunchingWithOptions: Unique Identifier read from Userdefaults %@", __FUNCTION__, _uuid);
    } else {
        // UUID not existing, generate a UUID
        self.uuid = [Toolbox getUniqueDeviceIdentifier];
        [defaults setObject:_uuid forKey:@"uuid"];
        [defaults synchronize];
        NSLog(@"%s: application: didFinishLaunchingWithOptions: Unique Identifier set in Userdefaults %@", __FUNCTION__, _uuid);
    }
}

#pragma mark - APNS Integration

/*
 * --------------------------------------------------------------------------------------------------------------
 *  BEGIN APNS CODE
 * --------------------------------------------------------------------------------------------------------------
 */

/**
 * Fetch and Format Device Token and Register Important Information to Remote Server
 */
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
	
    
    
    
#if !TARGET_IPHONE_SIMULATOR
    
    _theDeviceToken = devToken;
    
	// Get Bundle Info for Remote Registration (handy if you have more than one app)
	NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
	NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
	
	// Check what Notifications the user has turned on.  We registered for all three, but they may have manually disabled some or all of them.
	NSUInteger rntypes = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
	
	// Set the defaults to disabled unless we find otherwise...
	NSString *pushBadge = (rntypes & UIRemoteNotificationTypeBadge) ? @"enabled" : @"disabled";
	NSString *pushAlert = (rntypes & UIRemoteNotificationTypeAlert) ? @"enabled" : @"disabled";
	NSString *pushSound = (rntypes & UIRemoteNotificationTypeSound) ? @"enabled" : @"disabled";
	
    // Get the Unique ID
    NSLog(@"Unique-ID: %@", self.uuid);
    
	// Get the users Device Model, Display Name, Unique ID, Token & Version Number
	UIDevice *dev = [UIDevice currentDevice];
	NSString *deviceUuid;
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    deviceUuid = [defaults objectForKey:@"deviceUuid"];
    /*
     if ([dev respondsToSelector:@selector(uniqueIdentifier)])
     // deviceUuid = dev.uniqueIdentifier;
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     deviceUuid = [defaults objectForKey:@"deviceUuid"];
     else {
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     
     id uuid = [defaults objectForKey:@"deviceUuid"];
     if (uuid)
     deviceUuid = (NSString *)uuid;
     else {
     CFStringRef cfUuid = CFUUIDCreateString(NULL, CFUUIDCreate(NULL));
     deviceUuid = (NSString *)cfUuid;
     
     [defaults setObject:deviceUuid forKey:@"deviceUuid"];
     CFRelease(cfUuid);
     }
     }
     */
	NSString *deviceName = [dev.name stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
	NSString *deviceModel = [dev.model stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
	NSString *deviceSystemVersion = dev.systemVersion;
	
	// Prepare the Device Token for Registration (remove spaces and < >)
	NSString *deviceToken = [[[[devToken description]
                               stringByReplacingOccurrencesOfString:@"<"withString:@""]
                              stringByReplacingOccurrencesOfString:@">" withString:@""]
                             stringByReplacingOccurrencesOfString: @" " withString: @""];
	
    // Get the UserName if there is one
    NSString *theUsername = @"na";
    theUsername = [[SNKeychainWrapper sharedWrapper] objectForKey:(__bridge id)kSecAttrAccount];
    
	// Build URL String for Registration
	// !!! CHANGE "www.mywebsite.com" TO YOUR WEBSITE. Leave out the http://
	// !!! SAMPLE: "secure.awesomeapp.com"
	NSString *host = @"admin.catalog-viewer.de";
	
	// !!! CHANGE "/apns.php?" TO THE PATH TO WHERE apns.php IS INSTALLED
	// !!! ( MUST START WITH / AND END WITH ? ).
	// !!! SAMPLE: "/path/to/apns.php?"
	NSString *urlString = [NSString stringWithFormat:@"/APNS/php/apns.php?task=%@&appname=%@&appversion=%@&deviceuid=%@&devicetoken=%@&devicename=%@&devicemodel=%@&deviceversion=%@&pushbadge=%@&pushalert=%@&pushsound=%@&clientid=%@", @"register", appName,appVersion, deviceUuid, deviceToken, deviceName, deviceModel, deviceSystemVersion, pushBadge, pushAlert, pushSound, theUsername];
	
	// Register the Device Data
	// !!! CHANGE "http" TO "https" IF YOU ARE USING HTTPS PROTOCOL
	NSURL *url = [[NSURL alloc] initWithScheme:@"http" host:host path:urlString];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
	[NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    // ADDED RELEASE
    
    NSLog(@"Register URL: %@", url);
    
	
#endif
}

/**
 * Failed to Register for Remote Notifications
 */
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
	
#if !TARGET_IPHONE_SIMULATOR
	
	NSLog(@"Error in APNS registration. Error: %@", error);
	
#endif
}

/**
 * Remote Notification Received while application was open.
 */
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
	
#if !TARGET_IPHONE_SIMULATOR
    
	NSLog(@"remote notification: %@",[userInfo description]);
	NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
	
	NSString *alert = [apsInfo objectForKey:@"alert"];
	NSLog(@"Received Push Alert: %@", alert);
	
	NSString *sound = [apsInfo objectForKey:@"sound"];
	NSLog(@"Received Push Sound: %@", sound);
	
	NSString *badge = [apsInfo objectForKey:@"badge"];
	NSLog(@"Received Push Badge: %@", badge);
	application.applicationIconBadgeNumber = [[apsInfo objectForKey:@"badge"] integerValue];
	// Reset the Badge-Counter on App-Icon after Refresh has been done
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotification" object:nil userInfo:userInfo];
    
    
#endif
}


#pragma mark iOS 8
#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif

/*
 * --------------------------------------------------------------------------------------------------------------
 *  END APNS CODE
 * --------------------------------------------------------------------------------------------------------------
 */

@end
