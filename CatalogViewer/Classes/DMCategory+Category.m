//
//  DMCategory+Category.m
//  TestRestKit
//
//  Created by Шурик on 19.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "DMCategory+Category.h"

#import "DMManager.h"
#import "SNConnectionManager.h"

NSString * const DMCategoryIDKey            = @"cid";
NSString * const DMCategoryParentKey        = @"parent";
NSString * const DMCategoryTitleKey         = @"title";
NSString * const DMCategoryCompareFieldKey  = @"compareField";

@implementation DMCategory (Category)

+ (DMCategory *)categoryByID:(NSNumber *)categoryID inContext:(NSManagedObjectContext *)context
{
    DMCategory *category = nil;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMCategory class])];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K = %@", DMCategoryIDKey, categoryID];
    fetchRequest.fetchLimit = 1;
    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
    if ([results count]) {
        category = [results lastObject];
    }
    
    return category;
}


+ (DMCategory *)commonCategoryInContext:(NSManagedObjectContext *)context
{
	
	NSManagedObjectContext *defaultContext = [[DMManager sharedManager] defaultContext];
	[DMCategory favoritesCategoryInContext:defaultContext];
	
    DMCategory *categoryAll = [DMCategory categoryByID:@(0) inContext:defaultContext];
    if (categoryAll == nil) {
        categoryAll = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMCategory class]) inManagedObjectContext:defaultContext];
        categoryAll.title = NSLocalizedString(@"-- All --", nil);
        categoryAll.cid = @(0);
        NSError *error = nil;
        if (![defaultContext save:&error]) {
            NSLog(@"%s error: %@", __FUNCTION__, error);
        }
    }
	categoryAll.compareField = [NSString stringWithFormat:@"%d", -1];
	
    if (defaultContext != context) {
        categoryAll = (DMCategory *)[context objectWithID:categoryAll.objectID];
    }
    
    return categoryAll;
}

+ (DMCategory *)favoritesCategoryInContext:(NSManagedObjectContext *)context {
	NSManagedObjectContext *defaultContext = [[DMManager sharedManager] defaultContext];
	DMCategory *categoryFavorites = [DMCategory categoryByID:@(-1) inContext:defaultContext];
	if (categoryFavorites == nil) {
		categoryFavorites = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMCategory class]) inManagedObjectContext:defaultContext];
		categoryFavorites.cid = @(-1);
		NSError *error = nil;
		if (![defaultContext save:&error]) {
			NSLog(@"%s error: %@", __FUNCTION__, error);
		}
	}
	categoryFavorites.title = NSLocalizedString(@"My Favorites", nil);
	categoryFavorites.compareField = [NSString stringWithFormat:@"%d", 0];
	
	if (defaultContext != context) {
		categoryFavorites = (DMCategory *)[context objectWithID:categoryFavorites.objectID];
	}
	
	return categoryFavorites;
	
}

- (BOOL)isFavoriteCategory {
    return [self.cid isEqualToNumber: @(-1)];
}


+ (NSURLRequest *)requestCategories
{
    NSString *methodName = @"categories_v2.php";
    return [SNConnectionManager requestMethodName:methodName paramaters:nil requestType:SNURLReqeustTypeGet];
}


+ (void)parseResponse:(NSDictionary *)dictionary inContext:(NSManagedObjectContext *)context
{
    
    if (![[dictionary valueForKey:@"category"] isKindOfClass:[NSArray class]]) {
        return;
    }
    
    NSArray *array = [dictionary valueForKey:@"category"];
    NSManagedObjectContext *nestedContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    nestedContext.parentContext = context;
    
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([DMCategory class]) inManagedObjectContext:nestedContext];
    
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    
//    Parse parent categories
    [nestedContext performBlockAndWait:^{
    
        NSMutableSet *serverSet = [NSMutableSet setWithCapacity:[array count]];
        for (NSDictionary *entry in array) {
            NSInteger pid = [[entry valueForKey:@"parentID"] integerValue];
            if (pid != 0) {
                continue;
            }
            
            NSNumber *cid = @([[entry valueForKey:@"ID"] integerValue]);
            
//            NSLog(@"parent cid:%@ parentID:%ld", cid, (long)pid);
            
            DMCategory *category = [DMCategory categoryByID:cid inContext:nestedContext];
            if (!category) {
                category = [NSEntityDescription insertNewObjectForEntityForName:entity.name inManagedObjectContext:nestedContext];
                category.cid = cid;
            }
            
            category.title = [[entry valueForKey:@"title"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            category.compareField = category.title;
            
            NSError *error = nil;
            if (![nestedContext save:&error]) {
                NSLog(@"Error: %@", error);
            }
            
            [serverSet addObject:category];
        }
        
//        Parse subcategories
        for (NSDictionary *entry in array) {
            NSInteger pid = [[entry valueForKey:@"parentID"] integerValue];
            if (pid == 0) {
                continue;
            }
            
            NSNumber *cid = @([[entry valueForKey:@"ID"] integerValue]);
//            NSLog(@"child cid:%@ parentID:%ld", cid, (long)pid);
            DMCategory *category = [DMCategory categoryByID:cid inContext:nestedContext];
            if (!category) {
                category = [NSEntityDescription insertNewObjectForEntityForName:entity.name inManagedObjectContext:nestedContext];
                category.cid = cid;
            }
            category.title = [entry valueForKey:@"title"];
            
            DMCategory *parent = [DMCategory categoryByID:@(pid) inContext:nestedContext];
            if (parent) {
                [parent addSubcutegoriesObject:category];
            }
            
            category.compareField = [parent.compareField stringByAppendingString:category.title];
            
            NSError *error = nil;
            if (![nestedContext save:&error]) {
                NSLog(@"Error: %@", error);
            }
            
            [serverSet addObject:category];
        }
        
        DMCategory *commonCategory = [DMCategory commonCategoryInContext:nestedContext];
        [serverSet addObject:commonCategory];
        
        DMCategory *favorites = [DMCategory favoritesCategoryInContext:nestedContext];
        [serverSet addObject:favorites];
        
//        Delete unnessary sets
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMCategory class])];
        NSArray *results = [nestedContext executeFetchRequest:fetchRequest error:nil];
        if ([results count]) {
            NSMutableSet *localSet = [NSMutableSet setWithArray:results];
            [localSet minusSet:serverSet];
            for (DMCategory *category in localSet) {
                [nestedContext deleteObject:category];
            }
        }
        
        NSError *error = nil;
        if (![nestedContext save:&error]) {
            NSLog(@"Error: %@", error);
        } else {
            [context performBlock:^{
                NSError *error = nil;
                if (![context save:&error]) {
                    NSLog(@"Error: %@", error);
                }
            }];
        }
        
        dispatch_semaphore_signal(sema);
        
    }];
    
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
}

@end
