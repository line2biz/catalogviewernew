//
//  DMPublication+Category.m
//  CatalogViewer
//
//  Created by Шурик on 20.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "DMPublication+Category.h"

#import "SNConnectionManager.h"
#import "DMCategory+Category.h"

#import "ASIHTTPRequest.h"
#import "SSZipArchive.h"
#import "sys/xattr.h"

NSString * const DMPublicationIDKey = @"pubID";
NSString * const DMPublicationCrearedKey = @"created";
NSString * const DMPublicationCategoriesKey = @"categories";
NSString * const DMPublicationTitleKey = @"title";
NSString * const DMPublicationStoredKey = @"inLocalStore";

@implementation DMPublication (Category)

- (void)prepareForDeletion
{
    [self removeDownloadedPublication];
    [super prepareForDeletion];
}

+ (DMPublication *)publicationById:(NSNumber *)pid inContext:(NSManagedObjectContext *)context
{
    DMPublication *publication = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMPublication class])];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K = %@", DMPublicationIDKey, pid];
    fetchRequest.fetchLimit = 1;
    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
    if ([results count]) {
        publication = [results lastObject];
    }
    
    return publication;
}

+ (NSArray *)sortDescriptors
{
//    NSSortDescriptor *sortDesc1 = [[NSSortDescriptor alloc] initWithKey:DMPublicationStoredKey ascending:NO];
    NSSortDescriptor *sortDesc2 = [[NSSortDescriptor alloc] initWithKey:DMPublicationTitleKey ascending:YES];
    return @[sortDesc2];
}


+ (NSURLRequest *)requestPublications
{
    NSString *methodName = @"publications.php";
    return [SNConnectionManager requestMethodName:methodName paramaters:nil requestType:SNURLReqeustTypeGet];
}

+ (NSURLRequest *)requestRelations
{
    NSString *methodName = @"catrelations.php";
    return [SNConnectionManager requestMethodName:methodName paramaters:nil requestType:SNURLReqeustTypeGet];
}

+ (void)parseResponse:(NSDictionary *)dictionary inContext:(NSManagedObjectContext *)context
{
    
    if (![[dictionary valueForKey:@"publication"] isKindOfClass:[NSArray class]]) {
        return;
    }
    
    NSArray *array = [dictionary valueForKey:@"publication"];
    NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES];
    array = [array sortedArrayUsingDescriptors:@[sortDesc]];
    
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    
    NSManagedObjectContext *nestedContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    nestedContext.parentContext = context;
    [nestedContext performBlockAndWait:^{
        NSMutableSet *serverSet = [NSMutableSet setWithCapacity:[array count]];
        
        for (NSDictionary *entity in array) {
            NSNumber *pid = @([[entity valueForKey:@"ID"] integerValue]);
            
            DMPublication *publication = [DMPublication publicationById:pid inContext:nestedContext];
            if (publication == nil) {
                publication = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMPublication class]) inManagedObjectContext:nestedContext];
                publication.pubID = pid;
                publication.previewImage = [entity valueForKey:@"previewImage"];
                if (![publication initialiseNewPublication]) {
                    [nestedContext deleteObject:publication];
                    continue;
                }
            }
            
//            NSNumber *cid = @([[entity valueForKey:@"catID"] integerValue]);
//            DMCategory *category = [DMCategory getCategoryByID:cid inContext:nestedContext];
//            [publication addCategoriesObject:category];
            
            publication.activated = @([[entity valueForKey:@"activated"] integerValue]);
            
            NSString *sDate = [entity valueForKey:@"created"];
            NSDateFormatter *dateFormatter = [NSDateFormatter new];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            publication.created = [dateFormatter dateFromString:sDate];
            
            publication.downloadAllowed = @([[entity valueForKey:@"downloadAllowed"] integerValue]);
            publication.filesize = @([[entity valueForKey:@"filesize"] longLongValue]);
            publication.packageURL = [entity valueForKey:@"packageURL"];
            
            publication.pubType = @([[entity valueForKey:@"pubtype"] integerValue]);
            publication.title = [entity valueForKey:@"title"];
            publication.version = @([[entity valueForKey:@"userID"] integerValue]);
            publication.publicationStartLink = [entity valueForKey:@"publicationStartLink"];
            
//            NSLog(@"%@ pubtype %@ : %@", publication.pubID, publication.pubType, publication.publicationStartLink);
            
            [serverSet addObject:publication];
            
            NSError *error = nil;
            if (![nestedContext save:&error]) {
                NSLog(@"%s error: %@", __FUNCTION__, error);
            }
        }
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMPublication class])];
        NSArray *results = [nestedContext executeFetchRequest:fetchRequest error:nil];
        NSMutableSet *localSet = [NSMutableSet setWithArray:results];
        [localSet minusSet:serverSet];
        for (DMPublication *publication in localSet) {
            [nestedContext deleteObject:publication];
        }
        
        NSError *error = nil;
        if ([nestedContext hasChanges]) {
            [nestedContext save:&error];
        }
        
        if (error) {
            NSLog(@"%s %@", __FUNCTION__, error);
        } else  {
            
            if ([context hasChanges]) {
                [context performBlock:^{
                    NSError *error = nil;
                    if (![context save:&error]) {
                        NSLog(@"%s error: %@", __FUNCTION__, error);
                    }
                }];
            }
        }
        
        
        dispatch_semaphore_signal(sema);
    }];
    
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
}

+ (void)parseRelations:(NSDictionary *)dictoanry inContext:(NSManagedObjectContext *)context
{
    NSArray *entries = [dictoanry valueForKey:@"relations"];
    
    NSManagedObjectContext *nestedContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    
    if ([entries count] == 1) {
        if ([entries firstObject] == [NSNull null]) {
            return;
        }
    }
    
    nestedContext.parentContext = context;
    [nestedContext performBlockAndWait:^{
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMPublication class])];
        NSArray *results = [nestedContext executeFetchRequest:fetchRequest error:nil];
        for (DMPublication *publication in results) {
            
            NSMutableSet *mSetToRemove = [NSMutableSet new];
            for (DMCategory *category in publication.categories) {
                if (![category isFavoriteCategory]) {
                    [mSetToRemove addObject:category];
                }
            }
            [publication removeCategories:mSetToRemove];
        }
        
        
        [entries enumerateObjectsUsingBlock:^(NSDictionary *entry, NSUInteger idx, BOOL *stop) {
            NSInteger catID = [[entry valueForKey:@"catID"] integerValue];
            NSInteger pubID = [[entry valueForKey:@"pubID"] integerValue];
            DMCategory *category = [DMCategory categoryByID:@(catID) inContext:nestedContext];
            DMPublication *publication = [DMPublication publicationById:@(pubID) inContext:nestedContext];
            [publication addCategoriesObject:category];
            
            
        }];
        
        NSError *error = nil;
        if (![nestedContext save:&error]) {
            NSLog(@"%s error %@", __FUNCTION__, error);
            
        } else {
            [context performBlock:^{
                [context save:nil];
            }];
        }
        
    }];
    
}

- (NSString *)storePath
{
    static NSString *appPath = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        appPath = [[paths objectAtIndex:0] stringByDeletingLastPathComponent];
    });
    
    NSString *storePath = [NSString stringWithFormat:@"Documents/pub%@", self.pubID];
    return [appPath stringByAppendingPathComponent:storePath] ;
}

- (NSString *)thumbnailPath
{
    return [[self storePath] stringByAppendingPathComponent:@"thumbnail.png"];
}

- (BOOL)initialiseNewPublication
{
    int retState = 0;
    // First let's create the directory (publicationStore)
    NSString* storeName = [[NSString alloc] initWithFormat:@"pub%@", self.pubID];
    if ([self createPublicationStoreWithName:storeName]) {
        // The Store was successfully created
        // Now download the preview-Image
        NSString *appPath = [[[NSBundle mainBundle] bundlePath] stringByDeletingLastPathComponent];
        NSURL *url = [NSURL URLWithString:self.previewImage];
        
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request setDelegate:self];
        [request setDownloadDestinationPath:[self thumbnailPath]];
        
        NSLog(@"FileLogic: initialiseNewPublicationWithID: STORE-PATH: %@", [appPath stringByAppendingPathComponent:[self storePath]]);
        
        [request setNumberOfTimesToRetryOnTimeout:2];
        [request setTimeOutSeconds:5];
        [request startSynchronous];
        NSError *error = [request error];
        if (!error) {
            int statusCode = [request responseStatusCode];
            if (statusCode == 404) {
                NSLog(@"FileLogic: initialiseNewPublicationWithID: File was not found on server!");
                
                NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"default_thumbnail.png"];
                NSString *folderPath = [[self storePath] stringByAppendingPathComponent:@"thumbnail.png"];
                NSLog(@"FileLogic: initialiseNewPublicationWithID: Copying default-Thumbnail to local Store FROM: %@\n TO: %@", sourcePath, folderPath);
                // Deleting old file (contains garbage because we've recieved a 404 error)
                BOOL success = [[NSFileManager defaultManager] removeItemAtPath:folderPath error:&error];
                if (success) {
                    // File deleted
                }
                if([[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:folderPath error:&error]){
                    NSLog(@"FileLogic: initialiseNewPublicationWithID: Default-Thumbnail successfully copied");
                } else {
                    NSLog(@"FileLogic: initialiseNewPublicationWithID: Default-Thumbnail Error description-%@ \n", [error localizedDescription]);
                    NSLog(@"FileLogic: initialiseNewPublicationWithID: Default-Thumbnail Error reason-%@", [error localizedFailureReason]);
                }
                
                
                
                retState = FALSE;
            } else {
                // All went ok! File is downloaded!
                retState = TRUE;
                NSLog(@"FileLogic: initialiseNewPublicationWithID: PreviewImage was downloaded successfully!");
            }
        } else {
            // There's been an error downloading the thumbnail-Image
            // TO-DO
            retState = FALSE;
            NSLog(@"PreviewImage couldn't be downlaoded!!");
        }
    } else {
        retState = FALSE;
    }
    return retState;
}

- (BOOL)createPublicationStoreWithName:(NSString *)storeName
{
    int retState = NO;
    
    NSString *path = [self storePath];
    
    NSError *error;
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    }
    
    if ([[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:NO attributes:nil error:&error]) {
        if (![self addSkipBackupAttributeToItemAtURL:path]) {
            NSLog(@"ERROR: Could not set new Directory to be not backed up in iCloud and iTunes!!!!");
        };
        retState = TRUE;
        
    } else {
        NSLog(@"%s Cannot create directory error: %@", __FUNCTION__, error);
        retState = NO;
    }

    
    return retState;
}


/**
 *
 *  New in iOS 5.0.1
 *  Data stored in Documents-Directory should be set to
 *  "Do not Backup in iTunes or iCloud", if the data can
 *  be redownloaded or recreated!
 *
 *  following method will set the flag to the given directory
 *
 **/
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSString*)path

{
    const char* filePath = [path fileSystemRepresentation];
    const char* attrName = "com.apple.MobileBackup";
    u_int8_t attrValue = 1;
    
    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
    return result == 0;
}

- (void)removeDownloadedPublication
{
    NSString *path = [self storePath];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
        if (error) {
            NSLog(@"%s %@", __FUNCTION__, error);
        }
        
    }
}


@end
