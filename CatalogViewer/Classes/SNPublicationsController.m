//
//  FirstViewController.m
//  CatalogService
//
//  Created by is Industrie Software GmbH on 02.11.11.
//  Copyright 2011 is Industrie Software GmbH. All rights reserved.
//


#import "SNPublicationsController.h"


#import "SNLoginController.h"
#import "SNCategoriesController.h"


#import "SNAppDelegate.h"
#import "SNDownloadAllController.h"
#import "SNDownloadPublicationController.h"
#import "SNLoginController.h"
#import "SNSettingsController.h"
#import "SNPreviewController.h"
#import "DMManager.h"
#import "SNConnectionManager.h"
#import "SNKeychainWrapper.h"
#import "SVProgressHUD.h"
#import "SNPublicationCell.h"
#import "Reachability.h"
#import "BBBadgeBarButtonItem.h"




#import "math.h"
#import <QuartzCore/QuartzCore.h>
#import <AudioToolbox/AudioToolbox.h>
#import "BarButtonBadge.h"

#import "SNRecentSearchController.h"
#import "UIBarButtonItem+Badge.h"

#import "DMSharedPublications+CoreDataClass.h"

#import "SNSharedLinksVC.h"


NSString * const SNAppShowNewPublicationKey = @"SNAppShowNewPublicationKey";

@interface SNPublicationsController () <NSFetchedResultsControllerDelegate, UIPopoverControllerDelegate, SNLoginControllerDelegate, SNCategoriesControllerDelegate, SNSettingsControllerDeleate, UISearchBarDelegate, SNRecentSearchDelegate>
{
    __weak IBOutlet UIBarButtonItem *_categoriesBarButton;
    __weak IBOutlet UIBarButtonItem *_regrefeshBarButton;
    __weak IBOutlet UIBarButtonItem *_downloadBarButton;
    __weak IBOutlet UIBarButtonItem *_wifiBarButton;
    __weak IBOutlet UIBarButtonItem *_profileBarButton;
    __weak IBOutlet UILabel *_titleLabel;
    
    UILabel *_syncLabel;
    
    
    DMCategory *_currentCategory;
    
    // Audio
    SystemSoundID _theSSID;
    SystemSoundID _pageflipSSID;
    
    NSMutableArray *_objectChanges;
    NSMutableArray *_sectionChanges;
    SNCategoriesController *_categoriesController;
    
    // ===== Recent Search Controller =====
    SNRecentSearchController            *_recentSearchesController;
    UIPopoverController                 *_recentSearchesPopoverController;
    NSString                            *_searchString;
    NSPredicate                         *_predicate;
    
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSBlockOperation *blockOperation;
@property (assign, nonatomic) BOOL shouldReloadCollectionView;

@property (strong, nonatomic) DMCategory *currentCategory;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) UIPopoverController *popController;


@property (assign, nonatomic) BOOL umtsLoadingPermitted;
@property (assign, nonatomic) BOOL UMTSState;
@property (assign, nonatomic) BOOL wifiState;

@property (strong, nonatomic) Reachability *hostReachability;
@property (strong, nonatomic) Reachability *internetReachability;
@property (strong, nonatomic) Reachability *wifiReachability;

@property (strong, nonatomic) IBOutlet UILabel *emptyLabel;
@property (strong, nonatomic) BBBadgeBarButtonItem *barButtonBadge;
@property (strong, nonatomic) BBBadgeBarButtonItem *barButtonNew;
@property (strong, nonatomic) BBBadgeBarButtonItem *barButtonBadgeShare;

@end



@implementation SNPublicationsController

@dynamic currentCategory;


#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    //Change the host name here to change the server you want to monitor.
    ///TODO: Add Reachability
    NSString *remoteHostName = @"www.apple.com";
    
    self.hostReachability = [Reachability reachabilityWithHostName:remoteHostName];
	[self.hostReachability startNotifier];
	[self updateInterfaceWithReachability:self.hostReachability];
    
    self.internetReachability = [Reachability reachabilityForInternetConnection];
	[self.internetReachability startNotifier];
	[self updateInterfaceWithReachability:self.internetReachability];
    
    self.wifiReachability = [Reachability reachabilityForLocalWiFi];
	[self.wifiReachability startNotifier];
	[self updateInterfaceWithReachability:self.wifiReachability];
    
    
    
    self.wifiState = YES;
    
    self.managedObjectContext = [[DMManager sharedManager] defaultContext];
    self.currentCategory = [DMCategory commonCategoryInContext:self.managedObjectContext];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    
    
    
//    Badge View
//   Initialize NKNumberBadgeView…
    
    
    
    // Initialize UIBarbuttonitem…
    
    UIButton *customButton = [UIButton  buttonWithType:UIButtonTypeCustom];
    customButton.frame = CGRectMake(0, 0, 60, 30);
    [customButton setImage:[UIImage imageNamed:@"Icon-Download"] forState:UIControlStateNormal];
    //...
    
    // Create and add our custom BBBadgeBarButtonItem
    self.barButtonBadge = [[BBBadgeBarButtonItem alloc] initWithCustomUIButton:customButton];
    self.barButtonBadge.badgeBGColor = [UIColor colorWithRed:83/255.f green:154/255.f blue:60/255.f alpha:1];
    self.barButtonBadge.shouldAnimateBadge = YES;
    self.barButtonBadge.badgeOriginX = 42;
    self.barButtonBadge.badgeOriginY = 0;
    // Set a value for the badge
//    self.barButtonBadge.badgeValue = @"15009";
    [customButton addTarget:self action:@selector(didTapButtonLoadAll:) forControlEvents:UIControlEventTouchUpInside];
    
    

    
    // Create new publications bar button
    UIButton *btnNew = [UIButton buttonWithType:UIButtonTypeCustom];
    btnNew.frame = CGRectMake(0, 0, 60, 30);
    [btnNew setTitle:NSLocalizedString(@"New", nil) forState:UIControlStateNormal];
    [btnNew setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.barButtonNew = [[BBBadgeBarButtonItem alloc] initWithCustomUIButton:btnNew];
//    [btnNew addTarget:self action:@selector(didTapButtonShowNew:) forControlEvents:UIControlEventTouchUpInside];
    self.barButtonNew.badgeBGColor = [UIColor colorWithRed:83/255.f green:154/255.f blue:60/255.f alpha:1];
    self.barButtonNew.shouldAnimateBadge = YES;
    self.barButtonNew.badgeOriginX = 47;
    self.barButtonNew.badgeOriginY = 0;
//    self.barButtonNew.badgeValue = @"14";
    
    
    // create bar button badge for Share Action
    UINavigationBar *bar = self.navigationController.navigationBar;
    CGFloat height = CGRectGetHeight(bar.frame) * 3 / 5;
    
    UIButton *btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
    btnShare.frame = CGRectMake(0, 0, height, height);
    [btnShare setImage: [[UIImage imageNamed:@"Action-Share"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState: UIControlStateNormal];
    
    self.barButtonBadgeShare = [[BBBadgeBarButtonItem alloc] initWithCustomUIButton:btnShare];
    self.barButtonBadgeShare.badgeBGColor = [UIColor colorWithRed:83/255.f green:154/255.f blue:60/255.f alpha:1];
    self.barButtonBadgeShare.shouldAnimateBadge = YES;
    self.barButtonBadgeShare.badgeOriginX = height + 2;
    self.barButtonBadgeShare.badgeOriginY = 0;
    
    [btnShare addTarget:self action:@selector(openSharedLinksFromSender:) forControlEvents:UIControlEventTouchUpInside];
                                                   
    
    // Sync label
    _syncLabel = [UILabel new];
    _syncLabel.textColor = [UIColor whiteColor];
    _syncLabel.numberOfLines = 2;
    
    
    
    UIBarButtonItem *syncBarButton = [[UIBarButtonItem alloc] initWithCustomView:_syncLabel];
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 15.0f;
    
    NSLog(@"%d", _categoriesBarButton);
    NSLog(@"%d", _regrefeshBarButton);
    NSLog(@"%d", syncBarButton);
    NSLog(@"%d", self.barButtonNew);
    NSLog(@"%d", fixedItem);
    NSLog(@"%d", self.barButtonBadgeShare);
    
    self.navigationItem.leftBarButtonItems = @[
                                               _categoriesBarButton,
                                               _regrefeshBarButton,
                                               syncBarButton,
                                               self.barButtonBadge,
                                               self.barButtonNew,
                                               fixedItem,
                                               self.barButtonBadgeShare
                                               ];
    [self updateNavigationBarInfo];
    [_syncLabel sizeToFit];
    
    
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    
    NSString *str = @"v";
    str = [str stringByAppendingString:[[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"]];
    label.text = str;
    [label sizeToFit];
    UIBarButtonItem *versionBarButton = [[UIBarButtonItem alloc] initWithCustomView:label];
    

    UILabel *newLabel = [UILabel new];
    newLabel.text = NSLocalizedString(@"Show only new", nil);
    newLabel.textColor = [UIColor whiteColor];
    [newLabel sizeToFit];
    CGRect newLabelFrame = newLabel.frame;
    newLabelFrame.size.height = 31;
    newLabel.frame = newLabelFrame;
    
    UISwitch *newSwitch = [UISwitch new];
    newSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:SNAppShowNewPublicationKey];
    [newSwitch addTarget:self action:@selector(didTapButtonShowNew:) forControlEvents:UIControlEventValueChanged];
    CGRect newSwitchFrame = newSwitch.frame;
    newSwitchFrame.origin.x = CGRectGetMaxX(newLabel.frame) + 10;
    newSwitch.frame = newSwitchFrame;
    
    
    UIView *newView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetMaxX(newSwitchFrame), MAX(CGRectGetHeight(newLabel.frame), CGRectGetHeight(newSwitch.frame)))];
    [newView addSubview:newLabel];
    [newView addSubview:newSwitch];
    UIBarButtonItem *switchBarButton = [[UIBarButtonItem alloc] initWithCustomView:newView];
    
    
    
    
    
    
    self.navigationItem.rightBarButtonItems = @[_profileBarButton, _wifiBarButton, versionBarButton, switchBarButton];
    
//    Old School Code
//    self.navigationItem.leftBarButtonItems
    
    
    // Register ViewController to recieve PushNotifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushNotificationReceived:) name:@"pushNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateSharedLinksBadge) name:SNAppShouldUpdateSharedLinksCounter object:nil];
    
    
    // Setup Audio
    // refresh-sound
    NSURL *toneURLRef = [[NSBundle mainBundle] URLForResource:@"refresh" withExtension:@"caf"];
    SystemSoundID toneSSID = 0;
    AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
    _theSSID = toneSSID;
    // pageflip-sound
    toneURLRef = [[NSBundle mainBundle] URLForResource:@"pageflip" withExtension:@"caf"];
    _pageflipSSID = 0;
    AudioServicesCreateSystemSoundID( (__bridge CFURLRef)toneURLRef, &toneSSID );
    _pageflipSSID = toneSSID;
    
    
    
    
    // Check For Credentials in KeyChain

    //NSLog(@"FirstViewController: viewDidLoad: Loaded from Keychain: USERNAME: %@ PASSWORD: %@", theUsername, thePassword);
    // ADDED RELEASE
    //[keychain release];
    
    
    // Load UserDefaults
    // Get the stored data before the view loads
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([[defaults objectForKey:@"loadUMTS"] integerValue] == 1) {
        self.umtsLoadingPermitted = YES;
    } else {
        self.umtsLoadingPermitted = NO;
    }
    
    // ======= Recent Search =========
    _recentSearchesController = [[SNRecentSearchController alloc] initWithStyle:UITableViewStylePlain];
    _recentSearchesController.delegate = self;
    
    // Check for existance of Credentials
    
    [self configureView];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateSharedLinksBadge];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        if (![self credentialsAvailable]) {
//            [self performSegueWithIdentifier:NSStringFromClass([SNLoginController class]) sender:self];
//        }
//    });
    
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Controllers Methods
- (void)updateNavigationBarInfo {
    
    // Last update date
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = [NSDateFormatter dateFormatFromTemplate:@"dd.MM.yy HH:mm" options:0 locale:[NSLocale currentLocale]];
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] init];
    
    NSDictionary *attr1 = @{ NSFontAttributeName: [UIFont boldSystemFontOfSize:10]};
    [attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"Last sync", nil) attributes:attr1]];
    
    [attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n" attributes:nil]];
    
    NSDate *lastUpdate = [[NSUserDefaults standardUserDefaults] valueForKey:SNAppLastUpdateKey];
    if (lastUpdate == nil) {
        NSDictionary *attr2 = @{ NSFontAttributeName: [UIFont systemFontOfSize:10], NSForegroundColorAttributeName: [UIColor clearColor]};
        [attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:[dateFormatter stringFromDate:[NSDate date]] attributes:attr2]];
    } else {
        NSDictionary *attr2 = @{ NSFontAttributeName: [UIFont systemFontOfSize:10], NSForegroundColorAttributeName: [UIColor whiteColor]};
        [attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:[dateFormatter stringFromDate:lastUpdate] attributes:attr2]];
        
    }
    
    _syncLabel.attributedText = attrStr;
    
    // Count unread publications
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([DMPublication class])];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"unread == %@", @(YES)];
    NSUInteger qty = [self.managedObjectContext countForFetchRequest:fetchRequest error:nil];
    
    self.barButtonNew.badgeValue = [NSString stringWithFormat:@"%zd", qty];
    
}

- (void)configureView
{
    NSString *title = self.currentCategory.title;
    if (self.currentCategory.parent) {
        title = [self.currentCategory.parent.title stringByAppendingFormat:@" >> %@", self.currentCategory.title];
    }
    _titleLabel.text = title;
    
    [self updateSharedLinksBadge];
}

- (void)updateBadgeNumber
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:self.fetchedResultsController.fetchRequest.entityName];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K == 0", DMPublicationStoredKey];
    NSUInteger qty = [self.managedObjectContext countForFetchRequest:fetchRequest error:nil];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:qty];
    if (qty) {
        self.barButtonBadge.badgeValue = [NSString stringWithFormat:@"%lu", (unsigned long)qty];
        
//        self.numberBadge.value = qty;
//        self.numberBadge.hidden = NO;
    } else {
        self.barButtonBadge.badgeValue = @"";
//        self.numberBadge.value = 0;
//        self.numberBadge.hidden = YES;
    }
}

- (void)updateSharedLinksBadge
{
    DMSharedPublications *shared = [DMSharedPublications sharedInContext:nil];
    self.barButtonBadgeShare.badgeValue = [NSString stringWithFormat:@"%lu", (unsigned long)shared.publications.count];
}

- (void)openSharedLinksFromSender:(id)sender
{
    [self performSegueWithIdentifier:NSStringFromClass([SNSharedLinksVC class]) sender:self];
}

#pragma mark - Propety Accessors
- (void)setPopController:(UIPopoverController *)popController
{
    if (_popController) {
        [_popController dismissPopoverAnimated:NO];
        _categoriesController = nil;
    }
    
    _popController = popController;
}

- (DMCategory *)currentCategory
{
    if (_currentCategory == nil) {
        _currentCategory = [DMCategory commonCategoryInContext:self.managedObjectContext];
    }
    return _currentCategory;
}

- (void)setCurrentCategory:(DMCategory *)currentCategory
{
    if (_currentCategory != currentCategory) {
        _currentCategory = currentCategory;
//        _searchBar.text = nil;
        _fetchedResultsController = nil;
        [self.collectionView reloadData];
    }
}


#pragma mark - Segue Navigation
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:NSStringFromClass([SNCategoriesController class])]) {
        if (_categoriesController != nil) {
            [self.popController dismissPopoverAnimated:YES];
            self.popController = nil;
            return NO;
        }
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue isKindOfClass:[UIStoryboardPopoverSegue class]]) {
        self.popController = [(UIStoryboardPopoverSegue *)segue popoverController];
        self.popController.delegate = self;
    } else {
        self.popController = nil;
    }
    
    if ([[segue destinationViewController] isKindOfClass:[SNLoginController class]]) {
        [[segue destinationViewController] setDelegate:self];
    }
    else if ([[segue destinationViewController] isKindOfClass:[SNCategoriesController class]]) {
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
            self.popController.backgroundColor = [UIColor blackColor];
        }
        _categoriesController = (SNCategoriesController *)[segue destinationViewController];
        [[segue destinationViewController] setDelegate:self];
        [[segue destinationViewController] setManagedObjectContext:self.managedObjectContext];
        [[segue destinationViewController] setCurrentCategory:self.currentCategory];
        
    }
    else if ([[segue destinationViewController] isKindOfClass:[SNDownloadAllController class]]) {
        [[segue destinationViewController] setManagedObjectContext:self.managedObjectContext];
    
    }
    else if ([[segue destinationViewController] isKindOfClass:[SNDownloadPublicationController class]]) {
        [[segue destinationViewController] setPublication:(DMPublication *)sender];

    }
    else if ([[segue destinationViewController] isKindOfClass:[SNSettingsController class]]) {
        [[segue destinationViewController] setDelegate:self];
    
        
    }
    else if ([segue.identifier isEqualToString:NSStringFromClass([SNPreviewController class])]) {
        AudioServicesPlaySystemSound(_pageflipSSID);
        UINavigationController *navController = (UINavigationController *)[segue destinationViewController];
        SNPreviewController *previewController = (SNPreviewController *)[navController topViewController];
        previewController.managedObjectContext = self.managedObjectContext;
        DMPublication *publication = (DMPublication *)sender;
        if ([publication.unread boolValue] == YES) {
            [publication setValue:@(NO) forKey:@"unread"];
            [[DMManager sharedManager] saveContext];
        }
        [self updateNavigationBarInfo];
        NSIndexPath *indexPath = [self.fetchedResultsController indexPathForObject:publication];
        if (indexPath != nil) {
            [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
        }
        previewController.publication = (DMPublication *)sender;
        
        
    } else {
        UIViewController *viewController = [segue destinationViewController];
        NSLog(@"%s segue navigation: %@", __FUNCTION__, NSStringFromClass([viewController class]));
        
        viewController = viewController.parentViewController;
        NSLog(@"%s segue parent: %@", __FUNCTION__, NSStringFromClass([viewController class]));
        
    }

    
}


#pragma mark - Outler Methods
#pragma mark Buttons
- (IBAction)didTapButtonRefresh:(id)sender
{
    self.popController = nil;
    [self loadDataFromServer];
}

- (void)didTapButtonLoadAll:(id)sender
{
    [self performSegueWithIdentifier:NSStringFromClass([SNDownloadAllController class]) sender:self];
}

- (void)didTapButtonShowNew:(UISwitch *)sender {
    [[NSUserDefaults standardUserDefaults] setBool:sender.on forKey:SNAppShowNewPublicationKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    _fetchedResultsController = nil;
    [self.collectionView reloadData];
}

#pragma mark - Categories Controller Delegate
- (void)categoriesController:(SNCategoriesController *)categoriesController didSelectCategory:(DMCategory *)category
{
    [self.popController dismissPopoverAnimated:YES];
    self.popController = nil;
    self.currentCategory = category;
    [self configureView];
}



#pragma mark - Login Controller Delegate
- (void)loginController:(SNLoginController *)loginController didEnterWithParameters:(NSDictionary *)parametres
{
    [self.popController dismissPopoverAnimated:YES];
    self.popController = nil;
//    [self initialiseWorkspace];
    
}

#pragma mark - Settings Controller Delegate
- (void)settingsControllerDidSave:(SNSettingsController *)settingsController
{
    [self.popController dismissPopoverAnimated:YES];
    self.popController = nil;
    
    // Set UMTLoadingPermitted Setting
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"loadUMTS"]) {
        self.umtsLoadingPermitted = YES;
        if (self.UMTSState == YES) {
            _wifiBarButton.enabled = YES;
        }
    } else {
        self.umtsLoadingPermitted = NO;
        if (self.UMTSState == YES) {
            _wifiBarButton.enabled = NO;
        }
    }
    
}


#pragma mark - Popover Controller Delegate
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    if (popoverController == self.popController) {
        self.popController = nil;
    } else {
        [_searchBar resignFirstResponder];
    }
}

#pragma mark - Collection View Datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    NSInteger count = [[self.fetchedResultsController fetchedObjects] count];
    if (count == 0) {
        self.emptyLabel.hidden = NO;
    } else {
         self.emptyLabel.hidden = YES;
    }
    [self updateBadgeNumber];
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else
        return 0;
}

- (void)configureItem:(SNPublicationCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    DMPublication *publication = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.publication = publication;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    SNPublicationCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    [self configureItem:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - Collection View Delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    DMPublication *publication = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if ([publication.inLocalStore boolValue]) {
        [self performSegueWithIdentifier:NSStringFromClass([SNPreviewController class]) sender:publication];
    } else {
        [self performSegueWithIdentifier:NSStringFromClass([SNDownloadPublicationController class]) sender:publication];
    }
}


#pragma mark - Fetched results controller
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMPublication class])];
    [fetchRequest setFetchBatchSize:20];
    fetchRequest.sortDescriptors = [DMPublication sortDescriptors];;
    
    _predicate = nil;
    
    NSMutableArray *predicates = [NSMutableArray new];
    
    if (self.currentCategory != [DMCategory commonCategoryInContext:self.managedObjectContext]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY categories = %@ OR ANY categories.parent = %@",  self.currentCategory, self.currentCategory];
        [predicates addObject:predicate];
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:SNAppShowNewPublicationKey] == YES) {
        [predicates addObject:[NSPredicate predicateWithFormat:@"unread == %@", @(YES)]];
    }
    
    if ([_searchString length] > 0) {
        [predicates addObject:[NSPredicate predicateWithFormat:@"title contains[cd] %@", _searchString]];
    }
    
    if ([predicates count] > 1) {
        fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    } else if ([predicates count] == 1) {
        fetchRequest.predicate = [predicates firstObject];
    }
    
    _predicate = fetchRequest.predicate;
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}

#pragma mark - Fetched Results Controller Delegate
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.collectionView reloadData];
    [self updateBadgeNumber];
    
}


#pragma mark - Search results controller delegate method



- (void)filterResultsWithString:(NSString *)searchString
{
    _searchString = searchString;
    
//    NSPredicate *predicate = nil;
//    if ([searchString length] > 0) {
//        predicate = [NSPredicate predicateWithFormat:@"title contains[cd] %@", searchString];
//        
//        if (_predicate != nil) {
//            predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[ _predicate, predicate ]];
//        }
//        
//    } else {
//        predicate = _predicate;
//    }
//    
//    [self.fetchedResultsController.fetchRequest setPredicate:predicate];
//    [self.fetchedResultsController performFetch:nil];
    _fetchedResultsController = nil;
    [self.collectionView reloadData];
}

- (void)finishSearchWithString:(NSString *)searchString
{
    [self filterResultsWithString:searchString];
    
    [_recentSearchesPopoverController dismissPopoverAnimated:YES];
    _recentSearchesPopoverController = nil;
    
    if ([searchString length]) {
        _searchString = searchString;
    } else {
        _searchString = nil;
    }
    
    
    [self.collectionView reloadData];
    
    [_searchBar resignFirstResponder];
}



- (void)recentSearchesController:(SNRecentSearchController *)controller didSelectString:(NSString *)searchString
{
    _searchBar.text = searchString;
    [self finishSearchWithString:searchString];
}


#pragma mark - Search bar delegate methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    if (_recentSearchesPopoverController == nil) // create the popover if not already open
    {
        // Create a navigation controller to contain the recent searches controller,
        // and create the popover controller to contain the navigation controller.
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:_recentSearchesController];
        
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_1) {
            navigationController.navigationBar.barTintColor = [UIColor whiteColor];
        }
        
        
        UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:navigationController];
        _recentSearchesPopoverController = popover;
        _recentSearchesPopoverController.delegate = self;
        
        // Ensure the popover is not dismissed if the user taps in the search bar.
        popover.passthroughViews = [NSArray arrayWithObject:_searchBar];
        
        // Display the search results controller popover.
        [_recentSearchesPopoverController presentPopoverFromRect:[_searchBar bounds]
                                                          inView:_searchBar
                                        permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
        
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [_recentSearchesController filterResultsUsingString:searchText];
    [self filterResultsWithString:searchText];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if (_recentSearchesPopoverController != nil)
    {
        UINavigationController *navController = (UINavigationController *)_recentSearchesPopoverController.contentViewController;
        SNRecentSearchController *searchesController = (SNRecentSearchController *)navController.topViewController;
        if (searchesController.confirmSheet == nil)
        {
            [_recentSearchesPopoverController dismissPopoverAnimated:YES];
            _recentSearchesPopoverController = nil;
        }
    }
    [searchBar resignFirstResponder];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSString *searchString = [_searchBar text];
    [_recentSearchesController addToRecentSearches:searchString];
    [self finishSearchWithString:searchString];
    
    if ([[[self fetchedResultsController] fetchedObjects] count] == 0) {
        NSString *message = NSLocalizedString(@"No Results", nil);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
}

#pragma mark - Server Methods
- (void)loadDataFromServer
{
    if  ((self.wifiState == YES) || ((self.UMTSState == YES) && (self.umtsLoadingPermitted == YES))) {
        NSURLRequest *request = [DMCategory requestCategories];
//        NSString *status = NSLocalizedString(@"Collecting Categories ...", nil);
        [SVProgressHUD show];
        
        [SNConnectionManager performRequest:request completionHandler:^(id recievedData, NSURLResponse *response, NSError *error) {
            if (error) {
                [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
                
            } else {
                
                // additional check whether there were any categories loaded
                __block BOOL shouldProceedToRelations = YES; // flag for relations loading need
                if (![[((NSDictionary *)recievedData) valueForKey:@"category"] isKindOfClass:[NSArray class]]) {
                    
                    // there is no need no load any relations
                    // because it will only produce an error
                    shouldProceedToRelations = NO;
                }
                [DMCategory parseResponse:recievedData inContext:self.managedObjectContext];
                
                NSURLRequest *request = [DMPublication requestPublications];
                [SNConnectionManager performRequest:request completionHandler:^(id recievedData, NSURLResponse *response, NSError *error) {
                    if (error) {
                        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
                        
                    } else {
                        [DMPublication parseResponse:recievedData inContext:self.managedObjectContext];
                        
                        // check whether relations download is needed
                        if (shouldProceedToRelations == false) {
                            
                            // finish information download
                            [SVProgressHUD dismiss];
                            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:SNAppLastUpdateKey];
                            [self updateNavigationBarInfo];
                            return;
                        }
                        
                        // proceed to relations loading
                        NSURLRequest *request = [DMPublication requestRelations];
                        [SNConnectionManager performRequest:request completionHandler:^(id recievedData, NSURLResponse *response, NSError *error) {
                            
                            if (error) {
                                [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
                            } else {
                                [SVProgressHUD dismiss];
                                [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:SNAppLastUpdateKey];
                                [self updateNavigationBarInfo];
                                [DMPublication parseRelations:recievedData inContext:self.managedObjectContext];
                            }
                            
                        }];
                        
                        
                    }
                    
                    
                }];
                
            }
            
        }];
        
    } else {
        NSString *status = NSLocalizedString(@"No Wifi Network in reach.\nPlease connect to a wifi-Network or allow loading via UMTS.", nil);
        [SVProgressHUD showErrorWithStatus:status];

    }
}

- (BOOL)credentialsAvailable
{
    int retVal = NO;
    
    NSString *userName = [[SNKeychainWrapper sharedWrapper] objectForKey:(__bridge id)kSecAttrAccount];
    NSString *password = [[SNKeychainWrapper sharedWrapper] objectForKey:(__bridge id)kSecValueData];
    
    if  (([userName length] > 0) && ([password length] > 0)) {
        retVal = YES;
    } else {
        retVal = NO;
    }
    return retVal;
}

//PUBLICATIONSDOWNLOADDelegate



// PushNotifications
/*!
 @function	pushNotificationReceived
 @abstract	delegate gets called if a new pushNotofocation was recieved.
 @discussion refreshes all Data via REST (if Network is available)
 */
- (void)pushNotificationReceived: (NSNotification*)aNotification
{
    [self loadDataFromServer];
}

#pragma mark - Reachability
- (void) reachabilityChanged:(NSNotification *)note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
	[self updateInterfaceWithReachability:curReach];
}


- (void)updateInterfaceWithReachability:(Reachability *)reachability
{
    NetworkStatus status = [reachability currentReachabilityStatus];
    switch (status) {
        case kNotReachable:
            _wifiBarButton.enabled = NO;
            break;
        case kReachableViaWWAN:
            _wifiBarButton.enabled = YES;
            _wifiBarButton.image = [UIImage imageNamed:@"mobile"];
            break;
        
        case kReachableViaWiFi:
            _wifiBarButton.enabled = YES;
            _wifiBarButton.image = [UIImage imageNamed:@"Icon-Wifi"];
            break;
    }
}


@end

