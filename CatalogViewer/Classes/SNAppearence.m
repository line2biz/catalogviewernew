//
//  SNAppearence.m
//  CatalogViewer
//
//  Created by Шурик on 13.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNAppearence.h"

@implementation SNAppearence


+ (void)customizeAppearance
{
    UINavigationBar *navBarAppearence = [UINavigationBar appearance];
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        navBarAppearence.tintColor = [UIColor whiteColor];
        navBarAppearence.barTintColor = [UIColor blackColor];
    } else {
        navBarAppearence.barStyle = UIBarStyleBlack;
    }
    
}

+ (UIColor *)customGreenColor
{
    return [UIColor colorWithRed:83/255.f green:154/255.f blue:60/255.f alpha:1];
}

@end
