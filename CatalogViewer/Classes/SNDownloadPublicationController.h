//
//  SNDownloadPublicationController.h
//  CatalogViewer
//
//  Created by Шурик on 18.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>


@class DMPublication;

@interface SNDownloadPublicationController : UIViewController

@property (nonatomic, strong) DMPublication *publication;

@end

