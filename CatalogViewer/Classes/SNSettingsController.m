//
//  SNSettingsController.m
//  CatalogViewer
//
//  Created by Шурик on 18.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNSettingsController.h"

#import "SNKeychainWrapper.h"

@interface SNSettingsController ()

@property (weak, nonatomic) IBOutlet UITextField *userNameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UISwitch* loadUMTSSwitch;

@end

@implementation SNSettingsController

#pragma mark - View Life Cycle
//- (id)initWithCoder:(NSCoder *)aDecoder
//{
//    self = [super self];
//    if (self) {
//        self.contentSizeForViewInPopover = CGSizeMake(552.0, 350.0);
//    }
//    
//    return self;
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.userNameField.text = [[SNKeychainWrapper sharedWrapper] objectForKey:(__bridge id)kSecAttrAccount];
    self.passwordField.text = [[SNKeychainWrapper sharedWrapper] objectForKey:(__bridge id)kSecValueData];
    self.loadUMTSSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"loadUMTS"];
    
}


- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    self.view.superview.bounds = CGRectMake(0, 0, 552, 350);
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonSave:(id)sender
{
    NSString *userName = [[self.userNameField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] lowercaseString];
    NSString *password = [self.passwordField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [[SNKeychainWrapper sharedWrapper] setObject:userName forKey:(__bridge id)kSecAttrAccount];
    [[SNKeychainWrapper sharedWrapper] setObject:password forKey:(__bridge id)kSecValueData];
    
    [[NSUserDefaults standardUserDefaults] setBool:self.loadUMTSSwitch.on forKey:@"loadUMTS"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.delegate settingsControllerDidSave:self];
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end
