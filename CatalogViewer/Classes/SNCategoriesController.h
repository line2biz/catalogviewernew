//
//  SNCategoriesController.h
//  CatalogViewer
//
//  Created by Шурик on 13.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SNCategoriesControllerDelegate;

@class DMCategory;

@interface SNCategoriesController : UIViewController

@property (strong, nonatomic) DMCategory *currentCategory;
@property (weak, nonatomic) id<SNCategoriesControllerDelegate> delegate;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end

@protocol SNCategoriesControllerDelegate <NSObject>

- (void)categoriesController:(SNCategoriesController *)categoriesController didSelectCategory:(DMCategory *)category;

@end
