//
//  SNPreviewController.h
//  CatalogViewer
//
//  Created by Шурик on 18.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMPublication;

@interface SNPreviewController : UIViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) DMPublication *publication;


//@property (nonatomic, assign) NSInteger thePubID;
//@property (nonatomic, strong) NSString* theStartURL;
//@property (nonatomic, assign) NSInteger  downloadAllowed;

@end
