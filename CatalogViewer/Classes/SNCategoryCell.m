//
//  SNCategoryCell.m
//  CatalogViewer
//
//  Created by Шурик on 20.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNCategoryCell.h"

@implementation SNCategoryCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.textLabel.font = [UIFont fontWithName:@"GillSans" size:18];
    self.textLabel.textColor = [UIColor whiteColor];
    
    self.backgroundColor = [UIColor blackColor];
    self.contentView.backgroundColor = [UIColor blackColor];
}

@end
