//
//  SNSharedLinksVC.m
//  CatalogViewer
//
//  Created by Denis Harckevich on 04.07.17.
//  Copyright © 2017 Alexandr Zhovty. All rights reserved.
//

#import "SNSharedLinksVC.h"

#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "NSString+MD5.h"

#import "DMSharedPublications+CoreDataClass.h"
#import "DMPublication+Category.h"
#import "DMManager.h"
#import "SNSharedLinkTVC.h"

NSString * const SNAppShouldUpdateSharedLinksCounter = @"SNAppShouldUpdateSharedLinksCounter";

@interface SNSharedLinksVC () <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, MFMailComposeViewControllerDelegate, SNSharedLinkTVCDelegate>
{
    __weak IBOutlet UIButton *_buttonSend;
    __weak IBOutlet UIButton *_buttonClearAll;
    __weak IBOutlet UIButton *_cancel;
    
    __weak IBOutlet UITableView *_tableView;
}

@property (strong, nonatomic, nonnull) DMSharedPublications         *sharedPublication;
@property (strong, nonatomic, nonnull) NSManagedObjectContext       *managedObjectContext;
@property (strong, nonatomic, nonnull) NSFetchedResultsController   *fetchedResultsController;

@end

@implementation SNSharedLinksVC

#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.sharedPublication      = [DMSharedPublications sharedInContext:nil];
    self.managedObjectContext   = [[DMManager sharedManager] defaultContext];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    __autoreleasing NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"%s error: %@", __FUNCTION__, error);
    }
    else {
        [_tableView reloadData];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // save context
    __autoreleasing NSError *error = nil;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"%s error: %@", __FUNCTION__, error);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods
- (void)removeAllLinks
{
    [self.sharedPublication removePublications:self.sharedPublication.publications];
    [[NSNotificationCenter defaultCenter] postNotificationName:SNAppShouldUpdateSharedLinksCounter object:nil];
}

- (void)sendEmail
{
    // Send it via Mail
    // Email:
    NSLog(@"Composing URL - Mail");
    
    NSArray *aPublications = self.fetchedResultsController.fetchedObjects;
    if (aPublications != nil) {
        if (aPublications.count > 0) {
            
            MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
            picker.mailComposeDelegate = self;
            
            NSMutableString *emailBody = [NSMutableString stringWithString: @"Download-URLs:\r\n"];
            for (NSInteger i=0; i<aPublications.count; i++) {
                DMPublication *publication = aPublications[i];
                
                NSString *pub = [NSString stringWithFormat:@"%@", publication.pubID];
                [emailBody appendFormat:@"\r\n%@: http://download.catalog-viewer.de/dispatcher/?%@", publication.title, [pub md5]];
            }
            [emailBody appendString:@"\r\n \r\nIf the link does not work from your e-mail program, please copy it to the clipboard and paste it into the address bar of your preferred browser. \r\n \r\npowered by catalogviewer.de"];
            
            [picker setMessageBody:emailBody isHTML:NO];
            [self presentViewController:picker animated:YES completion:NULL];
        }
    }
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonSend:(id)sender
{
    [self sendEmail];
}

- (IBAction)didTapButtonClearAll:(id)sender
{
    NSString *msg = NSLocalizedString(@"Do You really want to remove all links from Share List?", nil);
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                             message:msg
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *remove = [UIAlertAction actionWithTitle:NSLocalizedString(@"Remove", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self removeAllLinks];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:remove];
    [alertController addAction:cancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)didTapButtonCancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Table View Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else
        return 0;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    DMPublication *publication = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    SNSharedLinkTVC *customCell = (SNSharedLinkTVC *)cell;
    customCell.labelTitle.text = publication.title;
    
    UIImage *theImage = [UIImage imageWithContentsOfFile:[publication thumbnailPath]];
    customCell.imageViewIcon.image = theImage;
    
    customCell.delegate = self;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIndentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIndentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}


#pragma mark - Fetched results controller
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMPublication class])];
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    fetchRequest.sortDescriptors = [DMPublication sortDescriptors];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"shared == %@", self.sharedPublication];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - Fetched Results Controller Delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = _tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView endUpdates];
}

#pragma mark - Mail Composer Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    if (result == MFMailComposeResultSent) {
        [self.sharedPublication removePublications:self.sharedPublication.publications];
        [[NSNotificationCenter defaultCenter] postNotificationName:SNAppShouldUpdateSharedLinksCounter object:nil];
        [controller dismissViewControllerAnimated:YES completion:^{
            [self didTapButtonCancel:nil];
        }];
        return;
    }
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Cell Protocol
- (void)deleteCell:(UITableViewCell *)cell
{
    NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
    if (indexPath != nil) {
        if (indexPath.row != NSNotFound) {
            DMPublication *publication = [self.fetchedResultsController objectAtIndexPath:indexPath];
            [self.sharedPublication removePublicationsObject:publication];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:SNAppShouldUpdateSharedLinksCounter object:nil];
        }
    }
}

@end




















