//
//  UIButtonLongTap.h
//  CatalogService
//
//  Created by is Industrie Software GmbH on 15.11.11.
//  Copyright (c) 2011 is Industrie Software GmbH. All rights reserved.
//



@protocol UIButtonLongTapDelegate

@optional

-(void)buttonDidLongTap:(NSNumber*)tag;
-(void)deleteSignalForTag:(NSNumber*)tag;

@end

@interface UIButtonLongTap : UIButton {
    
    id __weak delegate;
    BOOL consumedTap;
    
    UIImageView* newBadgeView;
    UIButton* deleteButton;
    BOOL showDeleteBadge;
    
    // tells if the NEW-Badge should be displayed
    BOOL isNew;
    // tells if the Image should be displayed as local (colored image) instead of remote (greyed image)
    BOOL isLocal;
}

@property (nonatomic, weak) id delegate;
@property (nonatomic, assign) BOOL showDeleteBadge;
@property (nonatomic, assign) BOOL isNew;
@property (nonatomic, assign) BOOL isLocal;



- (void)detectedLongTap;
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)showDeleteState:(BOOL)state;
- (void)showNewState:(BOOL)state;
- (void)addShadow;

@end
