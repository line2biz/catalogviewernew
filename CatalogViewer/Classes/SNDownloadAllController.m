//
//  SNDownloadController.m
//  CatalogViewer
//
//  Created by Шурик on 13.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNDownloadAllController.h"

#import "DMManager.h"
#import "HumanReadableDataSizeHelper.h"
#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"
#import "SSZipArchive.h"
#import "HumanReadableDataSizeHelper.h"
#import "Toolbox.h"

@interface SNDownloadAllController () <NSFetchedResultsControllerDelegate>
{
    
}

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *downloadButton;

@property (nonatomic, strong) IBOutlet UILabel* bytesDownloadedLabel;
@property (nonatomic, strong) IBOutlet UIProgressView*  queueProgressIndicator;


@property (strong, nonatomic) ASIHTTPRequest      *theRequest;
@property (nonatomic, strong) NSOperationQueue*         queue;
@property (strong)            ASINetworkQueue           *networkQueue;

@end

@implementation SNDownloadAllController

#pragma mark - View Life Cycle
#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureView];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    self.view.superview.bounds = CGRectMake(0, 0, 606, 282);
}

#pragma mark - Controller Methods
- (long long)allPublicationsSize
{
    if ([[self.fetchedResultsController fetchedObjects] count])
        return [[[self.fetchedResultsController fetchedObjects] valueForKeyPath:@"@sum.filesize"] longLongValue];
    else
        return 0;
}

- (void)configureView
{
    if ([[self.fetchedResultsController fetchedObjects] count]) {
        long long totalSize = [self allPublicationsSize];
        
        float freeSpace = [Toolbox getFreeSpace];
        if (freeSpace < totalSize) {
            NSString *str = NSLocalizedString(@"Not enougth available space left on your iPad. Remove unused Apps or documents and try again.", nil);
            self.bytesDownloadedLabel.text = str;
        } else {
            // Enougth Space left, inform the User about what to do to start the download
            NSString *str = NSLocalizedString(@"Hit the download button to download %@ ", nil);
            self.bytesDownloadedLabel.text = [NSString stringWithFormat:str, [Toolbox humanReadableFileSizeFromBytes:totalSize]];
        }
        
        self.downloadButton.enabled = YES;
        
    } else {
        NSString *text = NSLocalizedString(@"No pending downloads, all available publications where downlaoded to your iPad.", nil);
        self.bytesDownloadedLabel.text = text;
        
        self.downloadButton.enabled = NO;
    }
    
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonDownload:(id)sender
{
    NSLog(@"DownloadAllController :: downloadTapped");
    // Starting download of all files in Queue
    [self.cancelButton setTitle:NSLocalizedString(@"abort download", nil) forState:UIControlStateNormal];
    self.bytesDownloadedLabel.text = NSLocalizedString(@"Calculating overall size to download...", nil);
    self.downloadButton.enabled = NO;
    
    // Check if enougth Space in File System on Device
    float freeSpace = [Toolbox getFreeSpace];
    
    if (freeSpace < self.allPublicationsSize) {
        // Not enougth free space left
        NSLog(@"Not enougth space left on local storage to download all Publications. Free Space on Device: %f Needed Space: %lld", freeSpace, self.allPublicationsSize);
    } else {
        // Enougth space on local storage left, go on!
        NSLog(@"Checked for free Space on local storage, free Space on Device: %f needed Space: %lld", freeSpace, self.allPublicationsSize);
        // Start Download of publications
        
        // Create ASIHttp Download-Queue
        // Stop anything already in the queue before removing it
        [[self networkQueue] cancelAllOperations];
        
        // Creating a new queue each time we use it means we don't have to worry about clearing delegates or resetting progress tracking
        [self setNetworkQueue:[ASINetworkQueue queue]];
        [[self networkQueue] setDelegate:self];
        self.networkQueue.showAccurateProgress = YES;
        [[self networkQueue] setShouldCancelAllRequestsOnFailure:NO];
        [[self networkQueue] setRequestDidFinishSelector:@selector(requestFinished:)];
        [[self networkQueue] setRequestDidFailSelector:@selector(requestFailed:)];
        [[self networkQueue] setQueueDidFinishSelector:@selector(queueFinished:)];
        [[self networkQueue] setDownloadProgressDelegate:self];
        
        
        NSArray *array = [[self.fetchedResultsController fetchedObjects] sortedArrayUsingDescriptors:[DMPublication sortDescriptors]];
        for (int i = 0; i < [array count]; i++) {
            DMPublication *publication = array[i];
            
            // Start the Download
            NSURL *url = [NSURL URLWithString:[publication packageURL]];
            self.theRequest = [ASIHTTPRequest requestWithURL:url];
            
            if ([publication.pubType integerValue] == 1) {
                NSString* path = [[publication storePath] stringByAppendingPathComponent:@"package.zip"];
                [self.theRequest setDownloadDestinationPath:path];
                
            } else {
                NSString* path = [[publication storePath] stringByAppendingPathComponent:publication.publicationStartLink];
                [self.theRequest setDownloadDestinationPath:path];
            }
            
            self.theRequest.tag = [publication.pubID integerValue];
            [[self networkQueue] addOperation:self.theRequest];
        }
        
        // Start with processing the download queue
        [[self networkQueue] go];
        
    }

}

- (IBAction)didTapButtonCancel:(id)sender
{
    // Stop all Requests in Queue
    [self.networkQueue cancelAllOperations];
    [self.networkQueue reset];
    for (ASIHTTPRequest *req in [self.networkQueue operations]) {
        [req cancel];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table View Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else
        return 0;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    DMPublication *publication = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = publication.title;
    cell.detailTextLabel.text = [HumanReadableDataSizeHelper humanReadableSizeFromBytes:publication.filesize];
    UIImage *theImage = [UIImage imageWithContentsOfFile:[publication thumbnailPath]];
    cell.imageView.image = theImage;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIndentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIndentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}



#pragma mark - Fetched results controller
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMPublication class])];
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    fetchRequest.sortDescriptors = [DMPublication sortDescriptors];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K == 0", DMPublicationStoredKey];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}

#pragma mark - Fetched Results Controller Delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
    [self configureView];
}


#pragma mark - Fucking Old school methods
#pragma mark - Piece of shit
- (void)setCancelLabelText:(NSString*)theText {
    
    [self.cancelButton setTitle:theText forState:UIControlStateNormal];
}


/**
 *
 *  ASIHTTP Delegates
 *
 *
 **/


// Progress-Delegate for ASIHTTP
- (void)setProgress:(float)progress
{
    [self.queueProgressIndicator setProgress:progress];
    
    NSString* sizeLoaded = @"";
    NSString* sizeComplete = @"";
    unsigned long long tmpSize = 0;
    unsigned long long tmpSize2 = 0;
    
    if ((self.networkQueue.bytesDownloadedSoFar > 0) & (self.networkQueue.bytesDownloadedSoFar < 1024)) {
        // Bytes
        sizeLoaded = [NSString stringWithFormat:@"%llu Byte", self.networkQueue.bytesDownloadedSoFar];
    } else if ((self.networkQueue.bytesDownloadedSoFar >= 1024) & (self.networkQueue.bytesDownloadedSoFar < 1048576)) {
        // Kilobyte
        tmpSize = (self.networkQueue.bytesDownloadedSoFar / (1024));
        sizeLoaded = [NSString stringWithFormat:@"%llu Kilobyte", tmpSize];
    } else if ((self.networkQueue.bytesDownloadedSoFar >= (1048576)) & (self.networkQueue.bytesDownloadedSoFar < 1048576*1024)) {
        // Megabyte
        tmpSize = (self.networkQueue.bytesDownloadedSoFar / (1048576));
        sizeLoaded = [NSString stringWithFormat:@"%llu Megabyte", tmpSize];
    } else {
        // Gigabyte
        tmpSize = (self.networkQueue.bytesDownloadedSoFar / (1048576*1024));
        tmpSize2 = tmpSize * (1048576*1024);
        tmpSize2 = (self.networkQueue.totalBytesToDownload - tmpSize2) / (1048576);
        if (tmpSize2 > 99) {
            tmpSize2 = tmpSize2 / 10;
        }        sizeLoaded = [NSString stringWithFormat:@"%llu.%02llu Gigabyte", tmpSize, tmpSize2];
    }
    
    if ((self.networkQueue.totalBytesToDownload > 0) & (self.networkQueue.bytesDownloadedSoFar < 1024)) {
        // Bytes
        sizeComplete = [NSString stringWithFormat:@"%llu Byte", self.networkQueue.totalBytesToDownload];
    } else if ((self.networkQueue.bytesDownloadedSoFar >= 1024) & (self.networkQueue.totalBytesToDownload < 1048576)) {
        // Kilobyte
        tmpSize = (self.networkQueue.totalBytesToDownload / (1024));
        sizeComplete = [NSString stringWithFormat:@"%llu Kilobyte", tmpSize];
    } else if ((self.networkQueue.bytesDownloadedSoFar >= (1048576)) & (self.networkQueue.totalBytesToDownload < 1048576*1024)) {
        // Megabyte
        tmpSize = (self.networkQueue.totalBytesToDownload / (1048576));
        sizeComplete = [NSString stringWithFormat:@"%llu Megabyte", tmpSize];
    } else {
        // Gigabyte
        tmpSize = (self.networkQueue.totalBytesToDownload / (1048576*1024));
        tmpSize2 = tmpSize * (1048576*1024);
        tmpSize2 = (self.networkQueue.totalBytesToDownload - tmpSize2) / (1048576);
        if (tmpSize2 > 99) {
            tmpSize2 = tmpSize2 / 10;
        }
        
        sizeComplete = [NSString stringWithFormat:@"%llu.%02llu Gigabyte", tmpSize, tmpSize2];
        // Calculate the MB
    }
    
    self.bytesDownloadedLabel.text = [NSString stringWithFormat:@"%@ from %@ downloaded", sizeLoaded , sizeComplete ];
    
}

- (void)updateProgressView:(float)progress {
    NSLog(@"updating ProgressIndicator");
    //[queueProgressIndicator setProgress:progress];
    self.queueProgressIndicator.progress = progress;
}


// DELEGATES ASIHTTP
- (void)requestFinished:(ASIHTTPRequest *)request
{
    // If this was the last request set
    // The labels in UI to correct values
    if ([[self networkQueue] requestsCount] == 0) {
        [self performSelectorOnMainThread:@selector(setCancelLabelText:) withObject:@"please wait..." waitUntilDone:YES];
    }
    // Check for the returning status code
    // if other than 200 there's been a problem with the download
    
    if ([request responseStatusCode] == 200) {
        
        DMPublication *publication = [DMPublication publicationById:@(request.tag) inContext:self.managedObjectContext];
        if ([publication.pubType integerValue] == 1) {
            [SSZipArchive unzipFileAtPath:request.downloadDestinationPath toDestination:[publication storePath]];
            
        } else if ([publication.pubType integerValue] == 2) {
            
        }
        publication.inLocalStore = @(YES);
        [self.managedObjectContext save:nil];
        
    } else {
        NSLog(@"DownloadAllControllerViewController: requestFinished: Error while Downloading Publication! HTTP Response-Code is: %i", [request responseStatusCode]);
    }
    
    
    // We release the queue here if no requests left!
    if ([[self networkQueue] requestsCount] == 0) {
        [self setNetworkQueue:nil];
        NSLog(@"DownloadAllControllerViewController: requestFinished: Cleared the queue (released)");
    }
}


- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
    NSLog(@"Downlaod error: %@", [error localizedDescription]);
    // We release the queue here if no requests left!
    if ([[self networkQueue] requestsCount] == 0) {
        [self setNetworkQueue:nil];
        NSLog(@"Cleared the queue (released)");
    }
}

- (void)queueFinished:(ASINetworkQueue *)queue
{
    // You could release the queue here if you wanted
    if ([[self networkQueue] requestsCount] == 0) {
        [self setNetworkQueue:nil];
    }

    [self.cancelButton setTitle:NSLocalizedString(@"Close", nil) forState:UIControlStateNormal];
    
    self.bytesDownloadedLabel.text = @"all publications downloaded to your iPad.";
    NSLog(@"Queue finished. Cleared the queue (released)");
}



@end
