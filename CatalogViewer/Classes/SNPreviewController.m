//
//  SNPreviewController.m
//  CatalogViewer
//
//  Created by Шурик on 18.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNPreviewController.h"

#import "DMManager.h"

#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "NSString+MD5.h"
#import "DMSharedPublications+CoreDataClass.h"

#import "SNAppearence.h"

@interface SNPreviewController () <UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate, UIWebViewDelegate>
{
    __weak IBOutlet UIBarButtonItem *_homeBarButton;
    __weak IBOutlet UIBarButtonItem *_actionBarButton;
    __weak IBOutlet UIBarButtonItem *_cameraBarButton;
    __weak IBOutlet UIActivityIndicatorView *_activityIndicator;
	__weak IBOutlet UIBarButtonItem *_favoritesButton;
}

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end



@implementation SNPreviewController


#pragma mark - View Life Cycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Set user agent (Needed for the UIWebView because the JavaScript handles the dispays different for different browsers)
        NSDictionary *dictionnary = [[NSDictionary alloc] initWithObjectsAndKeys:@"Mozilla/5.0 (iPad; U; CPU OS 4_3_2 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3", @"UserAgent", nil];
        [[NSUserDefaults standardUserDefaults] registerDefaults:dictionnary];
        
//        [triangleButton setBackgroundImage:[UIImage imageNamed:@"button_bar_move.png"] forState:UIControlStateNormal];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
//    self.navigationItem.rightBarButtonItems = @[_cameraBarButton, _actionBarButton];
	
	[self configureFavoritesButton];
	
    UIImage *image = [UIImage imageNamed:@"Icon-HideShow"];
    CGRect frame = CGRectZero;
    frame.size = image.size;
    
    UIButton *buttonHide = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonHide setImage:image forState:UIControlStateNormal];
    buttonHide.frame = frame;
    [buttonHide addTarget:self action:@selector(didTapButtonHide:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = buttonHide;
    
    
    
//    NSString* pubPathComponent = [NSString stringWithFormat:@"pub%li", (long)self.thePubID];
//#if TARGET_IPHONE_SIMULATOR
//    NSLog(@"startURL: %@", self.theStartURL);
//    NSLog(@"downloadAllowed %li", (long)self.downloadAllowed);
//#endif
//    NSArray *paths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask ,YES );
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *path = [documentsDirectory stringByAppendingPathComponent:pubPathComponent];
//    //path = [path stringByAppendingPathComponent:@"index.html"];
//    path = [path stringByAppendingPathComponent:self.theStartURL];
//#if TARGET_IPHONE_SIMULATOR
//    NSLog(@"thePath: %@", path);
//#endif
    
    NSString *pathStr = [[self.publication storePath] stringByAppendingPathComponent:self.publication.publicationStartLink];
    NSURL *url = [NSURL fileURLWithPath:pathStr];
    self.webView.delegate = self;
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self  action:@selector(swipeRightAction:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    swipeRight.delegate = self;
    [self.webView addGestureRecognizer:swipeRight];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeftAction:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    swipeLeft.delegate = self;
    [self.webView addGestureRecognizer:swipeLeft];
    
    // Changed in Version 1.2.5
    //
    // UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(toggleNavigationBar:)];
    // swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
    // swipeDown.numberOfTouchesRequired = 2;
    // swipeDown.delegate = self;
    // [theWebView addGestureRecognizer:swipeDown];
    
    // Show Hide Controls
    // depending on publication-settings
//    if (self.publication.downloadAllowed downloadAllowed == 1) {
//        _actionBarButton.enabled = YES;
//    } else {
//    }
    _actionBarButton.enabled = [self.publication.downloadAllowed boolValue];
 
    // changed in Version 1.8
    [self configureShareButton];
}

#pragma mark - Private Methods
- (void)configureFavoritesButton {
	DMCategory *favorites = [DMCategory favoritesCategoryInContext:self.managedObjectContext];
	
	if ([favorites.publications containsObject:self.publication]) {
		_favoritesButton.image = [UIImage imageNamed:@"Icon-Star"];
        _favoritesButton.tintColor = [SNAppearence customGreenColor];
	} else {
		_favoritesButton.image = [UIImage imageNamed:@"Icon-Star-Empty"];
        _favoritesButton.tintColor = [UIColor whiteColor];
	}
}

- (void)configureShareButton {
    DMSharedPublications *shared = [DMSharedPublications sharedInContext:self.publication.managedObjectContext];
    if ([shared.publications containsObject:self.publication]) {
        [_actionBarButton setTintColor: [SNAppearence customGreenColor]];
    }
    else {
        [_actionBarButton setTintColor:[UIColor whiteColor]];
    }
}

- (void)simpleShare
{
    // Send it via Mail
    // Email:
    NSLog(@"Composing URL - Mail");
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    //[picker setSubject:@" "];
    
    //NSArray *toRecipients = [NSArray arrayWithObject:@" "];
    //[picker setToRecipients:toRecipients];
    
    NSString *pub = [NSString stringWithFormat:@"%@", self.publication.pubID];
    NSString *emailBody = [NSString stringWithFormat:@"Download-URL:\r\n \r\n http://download.catalog-viewer.de/dispatcher/?%@   \r\n \r\nIf the link does not work from your e-mail program, please copy it to the clipboard and paste it into the address bar of your preferred browser. \r\n \r\npowered by catalogviewer.de", [pub md5]];
    [picker setMessageBody:emailBody isHTML:NO];
    [self.navigationController presentViewController:picker animated:YES completion:NULL];
}

- (void)complexShare:(DMSharedPublications *)shared
{
    // check whether this publication is in Shared already
    if ([shared.publications containsObject:self.publication]) {
        
        // this publication is already in shared
        // propose its removal
        NSString *msg = NSLocalizedString(@"Would You like to remove this Publication from Shared List?", nil);
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:msg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *remove = [UIAlertAction actionWithTitle:NSLocalizedString(@"Remove", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [shared removePublicationsObject:self.publication];
            NSError *error = nil;
            if (![self.publication.managedObjectContext save:&error]) {
                NSLog(@"%s error: %@", __FUNCTION__, error);
            }
            else {
                [self configureShareButton];
            }
        }];
        [alertController addAction:cancel];
        [alertController addAction:remove];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else {
        // add this publication to shared
        [shared addPublicationsObject:self.publication];
        
        NSError *error = nil;
        if (![self.publication.managedObjectContext save:&error]) {
            NSLog(@"%s error: %@", __FUNCTION__, error);
        }
        else {
            [self configureShareButton];
        }
    }
}


#pragma mark - Outlet Methods
#pragma mark Buttons
- (IBAction)didTapButtonShow:(id)sender
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)didTapButtonHide:(id)sender
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (IBAction)didTapButtonHome:(id)sender
{
    NSURLRequest* stopRequest = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:@"about:blank"]];
    [self.webView loadRequest:stopRequest];
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)didTapButtonAction:(id)sender
{
    // check whether this publication is in Shared List already
    DMSharedPublications *shared = [DMSharedPublications sharedInContext:self.publication.managedObjectContext];
    if ([shared.publications containsObject:self.publication]) {
        
        // perform complex sharing action
        [self complexShare:shared];
    }
    else {
        // check which mode of sharing is needed: the multiple or single
        NSString *msg = NSLocalizedString(@"Would You like to add this Publication to Shared List or share it immediately?", nil);
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:msg preferredStyle:UIAlertControllerStyleAlert];
        
        // actions
        UIAlertAction *shareList = [UIAlertAction actionWithTitle:NSLocalizedString(@"Add to Share List", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self complexShare:shared];
        }];
        UIAlertAction *simpleShare = [UIAlertAction actionWithTitle:NSLocalizedString(@"Share now", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self simpleShare];
            });
        }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style: UIAlertActionStyleCancel handler:nil];
        
        [alertController addAction:shareList];
        [alertController addAction:simpleShare];
        [alertController addAction:cancel];
        
        // show alert
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (IBAction)didTapFavoritesButton:(id)sender {
	
	DMCategory *favorites = [DMCategory favoritesCategoryInContext:self.managedObjectContext];
	
	if ([favorites.publications containsObject:self.publication]) {
		[favorites removePublicationsObject:self.publication];
	} else {
		[favorites addPublicationsObject:self.publication];
	}
	
	[self configureFavoritesButton];
	
	[SNAppDelegate.sharedAppDelegate saveContext];
}



- (IBAction)didTapButtonCamera:(id)sender
{
    ///TODO: Camera Action
    UIGraphicsBeginImageContext(self.view.bounds.size);
	[self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
    
    
    
    // Send it via Mail
    // Email:
    NSLog(@"Composing Mail");
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	
	[picker setSubject:@"This is the subject"];
	
	NSArray *toRecipients = [NSArray arrayWithObject:@"email@email.com"];
	[picker setToRecipients:toRecipients];
	
	NSData *imageData = UIImageJPEGRepresentation(viewImage, 1);
	[picker addAttachmentData:imageData mimeType:@"image/jpg" fileName:@"Screenshot.jpg"];
	
	NSString *emailBody = @"Text on the body!";
	[picker setMessageBody:emailBody isHTML:YES];
    NSLog(@"Presenting Mail...");
	[self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)toggleNavigationBar:(id)sender {
#if TARGET_IPHONE_SIMULATOR
    NSLog(@"GESTURE: hiding/showing navigation BAR");
#endif
    ///TODO: Hide/Show Navigation Bar
//    if ([theToolbar isHidden]) {
//        NSLog(@"showing Navigation Bar");
//        
//        [UIToolbar beginAnimations:@"NavBarFadeIn" context:nil];
//        theToolbar.alpha = 0;
//        [theToolbar setHidden:NO];
//        [UIToolbar setAnimationCurve:UIViewAnimationCurveEaseIn];
//        [UIToolbar setAnimationDuration:1.0];
//        theToolbar.alpha = 0.7;
//        [UIToolbar commitAnimations];
//    } else {
//        NSLog(@"hiding Navigation Bar");
//        [UIToolbar beginAnimations:@"NavBarFadeOut" context:nil];
//        theToolbar.alpha = 0.7;
//        [UIToolbar setAnimationCurve:UIViewAnimationCurveEaseOut];
//        [UIToolbar setAnimationDuration:1.0];
//        theToolbar.alpha = 0;
//        [theToolbar setHidden:YES];
//        [UIToolbar commitAnimations];
//    }
}

#pragma mark Gesture Recognizer
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)swipeRightAction:(id)ignored
{
    //NSLog(@"Swipe Right");
    //add Function
}

- (void)swipeLeftAction:(id)ignored
{
    //NSLog(@"Swipe Left");
    //add Function
}

#pragma mark - Mail Composer Delegate
-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	[controller dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - Web View Delegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)req navigationType:(UIWebViewNavigationType)navigationType
{
    NSMutableURLRequest *request = (NSMutableURLRequest *)req;
    
    if ([request respondsToSelector:@selector(setValue:forHTTPHeaderField:)]) {
        /*
         Changed in 1.2.3
         [request setValue:[NSString stringWithFormat:@"Mozilla/5.0 (iPad; U; CPU OS 4_3_2 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3", [request valueForHTTPHeaderField:@"User-Agent"]] forHTTPHeaderField:@"User_Agent"];
         */
        [request setValue:@"Mozilla/5.0 (iPad; U; CPU OS 4_3_2 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3" forHTTPHeaderField:@"User_Agent"];
    }
    
    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [_activityIndicator stopAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_activityIndicator stopAnimating];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [_activityIndicator startAnimating];
}


@end
