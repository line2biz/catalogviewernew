//
//  DMCategory.m
//  CatalogViewer
//
//  Created by Шурик on 13.03.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "DMCategory.h"
#import "DMCategory.h"
#import "DMPublication.h"


@implementation DMCategory

@dynamic cid;
@dynamic compareField;
@dynamic title;
@dynamic parent;
@dynamic publications;
@dynamic subcutegories;

@end
