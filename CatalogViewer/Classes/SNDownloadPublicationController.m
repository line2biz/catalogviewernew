//
//  SNDownloadPublicationController.m
//  CatalogViewer
//
//  Created by Шурик on 18.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNDownloadPublicationController.h"

#import "ASIHTTPRequest.h"
#import "SSZipArchive.h"
#import "DMManager.h"
#import "SVProgressHUD.h"

#import <QuartzCore/QuartzCore.h>

@interface SNDownloadPublicationController () <ASIHTTPRequestDelegate>


@property (nonatomic, strong) IBOutlet UILabel*         titleLabel;
@property (nonatomic, strong) IBOutlet UILabel*         createdLabel;
@property (nonatomic, strong) IBOutlet UIProgressView*  progressView;
@property (nonatomic, strong) IBOutlet UIImageView*     imageView;


@property (nonatomic, strong) IBOutlet UIButton*        downloadButton;
@property (nonatomic, strong) IBOutlet UIButton*        cancelButton;
@property (nonatomic, strong) IBOutlet UILabel*         stateLabel;

@property (strong, nonatomic) NSString *theTitle;
@property (strong, nonatomic) ASIHTTPRequest *theRequest;

@property (assign, nonatomic) BOOL loading;


@end

@implementation SNDownloadPublicationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Prevent Device from going to sleep during downloading of publications
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    

    self.titleLabel.text = self.publication.title;
    
    UIImage *image = [[UIImage alloc] initWithContentsOfFile:[self.publication thumbnailPath]];
    [self.imageView setImage:image];
    // Format the Date shown
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    NSLocale *deLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"de_DE"];
    [dateFormatter setLocale:deLocale];
    self.createdLabel.text = [dateFormatter stringFromDate:self.publication.created];
    
    
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    self.view.superview.bounds = CGRectMake(0, 0, 600, 240);
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonCancel:(id)sender
{
    if (self.downloadButton.enabled) {
        [self dismissViewControllerAnimated:YES completion:NULL];
        
    } else {
        [self.theRequest cancel];
        self.stateLabel.text = @"Download cancelled...";
        self.stateLabel.hidden = NO;
        self.downloadButton.enabled = YES;
        self.cancelButton.enabled = YES;
        
    }
    
    
}

- (IBAction)didTapButtonDownload:(id)sender
{
    // Show the WorkState Label to the User
    self.stateLabel.text = NSLocalizedString(@"Publication is downloading from server...", nil);
    self.stateLabel.hidden = NO;
    self.downloadButton.enabled = NO;
    self.cancelButton.enabled = YES;
    
    
    // Start the Download
    NSURL *url = [NSURL URLWithString:self.publication.packageURL];
    self.theRequest = [ASIHTTPRequest requestWithURL:url];
    [self.theRequest setDelegate:self];
    
    NSUInteger publicationType = [self.publication.pubType integerValue];
    
    if (publicationType == 1) {
        NSString* path = [[self.publication storePath] stringByAppendingPathComponent:@"package.zip"];
        [self.theRequest setDownloadDestinationPath:path];
        
    } else {
        NSString* path = [[self.publication storePath] stringByAppendingPathComponent:self.publication.publicationStartLink];
        [self.theRequest setDownloadDestinationPath:path];
        
    }

    [self.theRequest setDownloadProgressDelegate:self];
    [self.theRequest startAsynchronous];
}

#pragma mark - Old school methods
-(NSString *)documentsDirectoryPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectoryPath = [paths objectAtIndex:0];
    return documentsDirectoryPath;
}

- (void)showUNZIPMessage
{
    self.stateLabel.text = @"Unpacking Publication for you, Please wait ...";
//    [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
}

- (void)hideUNZIPMessage {
    //[self showUnZIPHud:[NSNumber numberWithInteger:0]];
}

// Progress-Delegate for ASIHTTP
- (void)setProgress:(float)progress
{
    [self.progressView setProgress:progress];
}



// DELEGATES ASIHTTP
- (void)requestFinished:(ASIHTTPRequest *)request
{
    // Check for the returning status code
    // if other than 200 there's been a problem with the download
    
    NSLog(@"%s responseStatusCode: %d", __FUNCTION__, [request responseStatusCode]);
    
    if ([request responseStatusCode] == 200) {
        
        
        NSLog(@"%s Download complete! HTTP-Response-Code: %i", __FUNCTION__, [request responseStatusCode]);
        NSLog(@"%@", [self.publication storePath]);
        
        if ([self.publication.pubType integerValue] == 1) {
            [self performSelectorOnMainThread:@selector(showUNZIPMessage) withObject:nil waitUntilDone:YES];
            [SSZipArchive unzipFileAtPath:request.downloadDestinationPath toDestination:[self.publication storePath]];
            
        }
        self.publication.inLocalStore = @(YES);
        [self.publication.managedObjectContext save:nil];
        
        __weak typeof(self) weakSelf = self;
        double delayInSeconds = 1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [weakSelf dismissViewControllerAnimated:YES completion:NULL];
        });
        
        
    } else {
        NSLog(@"Error while Downloading Publication! HTTP Response-Code is: %i", [request responseStatusCode]);
        NSString *str = NSLocalizedString(@"Die Publikation ist derzeit nicht abrufbar! Bitte versuchen Sie es zu einem späteren Zeitpunkt erneut (%i)", nil);
        self.stateLabel.text = [NSString stringWithFormat:str, [request responseStatusCode]];
    }
    
    self.downloadButton.enabled = YES;
    self.cancelButton.enabled = YES;
}



- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
    NSLog(@"Downlaod error: %@", [error localizedDescription]);
}

@end
