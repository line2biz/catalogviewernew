//
//  SNSharedLinksVC.h
//  CatalogViewer
//
//  Created by Denis Harckevich on 04.07.17.
//  Copyright © 2017 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

FOUNDATION_EXTERN NSString * const SNAppShouldUpdateSharedLinksCounter;

@interface SNSharedLinksVC : UIViewController

@end
