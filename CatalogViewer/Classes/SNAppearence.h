//
//  SNAppearence.h
//  CatalogViewer
//
//  Created by Шурик on 13.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNAppearence : NSObject

+ (void)customizeAppearance;

+ (UIColor *)customGreenColor;

@end
