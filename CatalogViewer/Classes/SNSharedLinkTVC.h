//
//  SNSharedLinkTVC.h
//  CatalogViewer
//
//  Created by Denis Harckevich on 04.07.17.
//  Copyright © 2017 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SNSharedLinksVC;

@protocol SNSharedLinkTVCDelegate;

@interface SNSharedLinkTVC : UITableViewCell

@property (weak, nonatomic, nullable) id <SNSharedLinkTVCDelegate> delegate;

@property (weak, nonatomic, nullable) IBOutlet UIImageView *imageViewIcon;
@property (weak, nonatomic, nullable) IBOutlet UILabel     *labelTitle;

@end

@protocol SNSharedLinkTVCDelegate <NSObject>

- (void)deleteCell:(UITableViewCell * _Nonnull)cell;

@end
