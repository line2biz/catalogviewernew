//
//  DMCategory.h
//  CatalogViewer
//
//  Created by Шурик on 13.03.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DMCategory, DMPublication;

@interface DMCategory : NSManagedObject

@property (nonatomic, retain) NSNumber * cid;
@property (nonatomic, retain) NSString * compareField;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) DMCategory *parent;
@property (nonatomic, retain) NSSet *publications;
@property (nonatomic, retain) NSSet *subcutegories;
@end

@interface DMCategory (CoreDataGeneratedAccessors)

- (void)addPublicationsObject:(DMPublication *)value;
- (void)removePublicationsObject:(DMPublication *)value;
- (void)addPublications:(NSSet *)values;
- (void)removePublications:(NSSet *)values;

- (void)addSubcutegoriesObject:(DMCategory *)value;
- (void)removeSubcutegoriesObject:(DMCategory *)value;
- (void)addSubcutegories:(NSSet *)values;
- (void)removeSubcutegories:(NSSet *)values;

@end
