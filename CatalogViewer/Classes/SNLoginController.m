//
//  SNLoginController.m
//  CatalogViewer
//
//  Created by Шурик on 09.02.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNLoginController.h"

#import "SNKeychainWrapper.h"


@interface SNLoginController ()
@property (nonatomic, strong) IBOutlet UITextField *userNameField;
@property (nonatomic, strong) IBOutlet UITextField *passwordField;
@end

@implementation SNLoginController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.userNameField.text = [[SNKeychainWrapper sharedWrapper] objectForKey:(__bridge id)kSecAttrAccount];
    self.passwordField.text = [[SNKeychainWrapper sharedWrapper] objectForKey:(__bridge id)kSecValueData];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    self.view.superview.bounds = CGRectMake(0, 0, 552, 384);
}

- (void)dealloc
{
    NSLog(@"%s", __FUNCTION__);
}

#pragma mark - Status Bar
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonEnter:(id)sender
{
    NSString *userName = [[self.userNameField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] lowercaseString];
    NSString *password = [self.passwordField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    [[SNKeychainWrapper sharedWrapper] setObject:@"Myappstring" forKey:(__bridge id)kSecAttrService];
    [[SNKeychainWrapper sharedWrapper] setObject:userName forKey:(__bridge id)kSecAttrAccount];
    [[SNKeychainWrapper sharedWrapper] setObject:password forKey:(__bridge id)kSecValueData];
    
    [self.delegate loginController:self didEnterWithParameters:nil];
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

@end
