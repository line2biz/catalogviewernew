//
//  SNConnectionManager.m
//


#import "SNConnectionManager.h"

#import "Base64.h"
#import "SNKeychainWrapper.h"

@import ObjectiveC.runtime;


#define CONNECTION_MANAGER_DOMAIN   @"com.vidcam.connection"
#define CONNECTION_RECIEVED_DATA    @"CONNECTION_RECIEVED_DATA"
#define CONNECTION_ERROR_BLOCK      @"CONNECTION_ERROR_BLOCK"
#define CONNECTION_COMPLETE_BLOCK   @"CONNECTION_COMPLETE_BLOCK"
#define CONNECTION_DOWNLOAD_BLOCK   @"CONNECTION_DOWNLOAD_BLOCK"
#define CONNECTION_EXPECTED_LENGTH  @"CONNECTION_EXPECTED_LENGTH"
#define CONNECTION_HTTP_RESPONSE    @"CONNECTION_HTTP_RESPONSE"


NSString *const SNConnectionExpectedContentLengthKey    = @"SNConnectionExpectedContentLengthKey";
NSString *const SNConnectionReceivedContentLengthKey    = @"SNConnectionReceivedContentLengthKey";
NSString *const SNConnectionDidReceiveDataEvent         = @"SNConnectionDidReceiveDataEvent";
NSString *const SNConnectionAPIServerURIValue           = @"http://admin.catalog-viewer.de/REST/ipad/";



typedef void (^ConnectionCompletionHandler)(id recievedData, NSURLResponse *response, NSError *error);


@interface SNConnectionManager()
{
    NSOperationQueue *_queue;
    NSMutableDictionary *_connections;
    NSCache *_imageCache;
    BOOL _NoUserExceptionFlag;
    
}

@property (strong, nonatomic) NSMutableArray *activeConnections;

@end


@implementation SNConnectionManager

#pragma mark - Request
+ (NSURLRequest *)requestMethodName:(NSString *)methodName paramaters:(NSDictionary *)parameters requestType:(SNURLReqeustType)requestType;
{
    NSString *str = [SNConnectionAPIServerURIValue stringByAppendingString:methodName];
    NSURL *url = [NSURL URLWithString:str];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSString *userName = [[SNKeychainWrapper sharedWrapper] objectForKey:(__bridge id)kSecAttrAccount];
    NSString *password = [[SNKeychainWrapper sharedWrapper] objectForKey:(__bridge id)kSecValueData];

    NSString *authorizationValue = [NSString stringWithFormat:@"%@:%@", userName, password];
    authorizationValue = [authorizationValue base64EncodedString];
    authorizationValue = [@"Basic " stringByAppendingString:authorizationValue];
    [request setValue:authorizationValue forHTTPHeaderField:@"Authorization"];
    
    return request;
}

+ (NSString *)queryStringFromDictioanry:(NSDictionary *)dictionary
{
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:dictionary.count];
    for (id key in dictionary) {
        NSString *str = [NSString stringWithFormat:@"%@=%@", key, dictionary[key]];
        [array addObject:str];
    }
    
    if ([array count])
        return [array componentsJoinedByString:@"&"];
    else
        return nil;
}


#pragma mark - Initiliazation

- (id)init
{
    self = [super init];
    if (self) {
        _queue = [[NSOperationQueue alloc] init];
        _queue.name = CONNECTION_MANAGER_DOMAIN;
        _connections = [NSMutableDictionary new];
        _imageCache = [[NSCache alloc] init];
        [_queue setMaxConcurrentOperationCount:10];
    }
    
    return self;
}


+ (SNConnectionManager *)sharedManager
{
    static dispatch_once_t once;
    static SNConnectionManager *__instance;
    dispatch_once(&once, ^ { __instance = [[SNConnectionManager alloc] init]; });
    return __instance;
}




- (NSURLConnection *)performRequest:(NSURLRequest *)request completionHandler:(ConnectionCompletionHandler)completionHandler
{
    if (request == nil) {
        return nil;
    }
    
    if ([_connections valueForKey:[[request URL] absoluteString]]) {
        return nil;
    }
    
    NSString *key = [[request URL] absoluteString];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    if (completionHandler) {
        objc_setAssociatedObject(connection, CONNECTION_COMPLETE_BLOCK, completionHandler, OBJC_ASSOCIATION_COPY_NONATOMIC);
    }
    
    if (key != nil) {
        [_connections setObject:connection forKey:[[request URL] absoluteString]];
    }
    
    [connection setDelegateQueue:_queue];
    
    
    
    
    [_queue addOperationWithBlock:^{
        [self startConnection:connection];
    }];

    return connection;
}

#pragma mark - Request
+ (NSURLRequest *)requestForURLString:(NSString *)urlString paramaters:(NSDictionary *)parameters
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    
    
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:parameters];
    [params setValue:@"12345" forKey:@"tokenString"];
    
    
    NSMutableArray *arrParams = [NSMutableArray arrayWithCapacity:[params count]];
    for (id key in params) {
        [arrParams addObject:[NSString stringWithFormat:@"%@=%@", key, [params valueForKey:key]]];
    }
    
    //    NSLog(@"%s %@", __FUNCTION__, [params description]);
    
    [request setHTTPMethod:@"POST"];
    NSString *bodyString = [arrParams componentsJoinedByString:@"&"];
    [request setHTTPBody:[bodyString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *contentLength = [NSString stringWithFormat:@"%lu", (unsigned long)[request.HTTPBody length]];
    [request setValue:contentLength forHTTPHeaderField:@"Content-Length"];
    
    return request;
}




#pragma mark - Public Functions

+ (NSURLConnection *)performRequest:(NSURLRequest *)request completionHandler:(ConnectionCompletionHandler)completionHandler
{
    return [[SNConnectionManager sharedManager] performRequest:request completionHandler:completionHandler];
}

+ (void)stopAllConnections
{
    [[SNConnectionManager sharedManager] stopAllConnections];
}

+ (void)stopConnection:(NSURLConnection *)connection
{
    SNConnectionManager *manager = [SNConnectionManager sharedManager];
    [manager.activeConnections enumerateObjectsUsingBlock:^(NSURLConnection *conn, NSUInteger idx, BOOL *stop) {
        if (connection == conn) {
            NSLog(@"%s", __FUNCTION__);
            [manager stopConnection:connection];
            *stop = YES;
        }
    }];
}

#pragma mark - Private Function
- (void)startConnection:(NSURLConnection *)connection
{
    [_activeConnections addObject:connection];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [connection start];
}

- (void)stopConnection:(NSURLConnection *)connection
{
    [connection cancel];
    
    objc_setAssociatedObject(connection, CONNECTION_RECIEVED_DATA,   nil, OBJC_ASSOCIATION_RETAIN);
    objc_setAssociatedObject(connection, CONNECTION_COMPLETE_BLOCK, nil, OBJC_ASSOCIATION_RETAIN);
    objc_setAssociatedObject(connection, CONNECTION_ERROR_BLOCK,    nil, OBJC_ASSOCIATION_RETAIN);
    
    [_activeConnections removeObject:connection];
    connection = nil;
    
    if ([_activeConnections count] == 0) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
    
}

- (void)stopAllConnections
{
    [_activeConnections makeObjectsPerformSelector:@selector(cancel)];
    for (NSURLConnection *connection in _activeConnections) {
        [self stopConnection:connection];
    }
    [_queue cancelAllOperations];
}

#pragma mark - Notification

- (void)postConnectionNotifcation:(NSURLConnection *)connection expectedLength:(NSNumber *)expectedLength receivedLength:(NSNumber *)recievedLength
{
    NSDictionary *userInfo = @{SNConnectionExpectedContentLengthKey : expectedLength, SNConnectionReceivedContentLengthKey : recievedLength };
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:SNConnectionDidReceiveDataEvent object:connection userInfo:userInfo];
    });
}

#pragma mark - Deprecated authentication delegates.
//- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
//{
//    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
//}


//- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
//{
//    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
//    {
//        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
//    }
//    else
//    {
//        [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
//    }
//}

#pragma mark - NSURLConnection delegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    if ([httpResponse statusCode] >= 400) {
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey : [NSHTTPURLResponse localizedStringForStatusCode:[httpResponse statusCode]]};
        NSError *error = [[NSError alloc] initWithDomain:CONNECTION_MANAGER_DOMAIN code:[httpResponse statusCode] userInfo:userInfo];
        [self connection:connection didFailWithError:error];
        
    }
    
    objc_setAssociatedObject(connection, CONNECTION_HTTP_RESPONSE, response, OBJC_ASSOCIATION_RETAIN);
    
    NSNumber *number = [NSNumber numberWithLongLong:[response expectedContentLength]];
    objc_setAssociatedObject(connection, CONNECTION_EXPECTED_LENGTH, number, OBJC_ASSOCIATION_RETAIN);
    objc_setAssociatedObject(connection, CONNECTION_RECIEVED_DATA,[NSMutableData new], OBJC_ASSOCIATION_RETAIN);
    
    [self postConnectionNotifcation:connection expectedLength:number receivedLength:@(0)];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSMutableData *responseData = (NSMutableData *)objc_getAssociatedObject(connection, CONNECTION_RECIEVED_DATA);
    [responseData appendData:data];
    
    NSNumber *expexted = objc_getAssociatedObject(connection, CONNECTION_EXPECTED_LENGTH);
    [self postConnectionNotifcation:connection expectedLength:expexted receivedLength:@([responseData length])];

    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSString *key = [[[connection currentRequest] URL] absoluteString];
    if (key != nil) {
        [_connections removeObjectForKey:key];
    }
    
    NSError *err = error;
    
    if (error.code == -1003) {
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey : @"Verbindung zum Server fehlgeschlagen."};
        err = [[NSError alloc] initWithDomain:CONNECTION_MANAGER_DOMAIN code:-1004 userInfo:userInfo];
        
    } else if (error.code == -1004) {
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey : @"Keine Netzwerkverbindung."};
        err = [[NSError alloc] initWithDomain:CONNECTION_MANAGER_DOMAIN code:error.code userInfo:userInfo];
    }
    
    NSURLResponse *response = (NSURLResponse *)objc_getAssociatedObject(connection, CONNECTION_HTTP_RESPONSE);
    ConnectionCompletionHandler block = objc_getAssociatedObject(connection, CONNECTION_COMPLETE_BLOCK);
    if (block != NULL) {
        dispatch_async(dispatch_get_main_queue(), ^{
            block(nil, response, err);
        });
    }
    
//    NSLog(@"%s ")
    
    [self stopConnection:connection];
}

- (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSString *key = [[[connection currentRequest] URL] absoluteString];
    if (key != nil) {
        [_connections removeObjectForKey:key];
    }
    
    
    NSMutableData *recievedData = (NSMutableData *)objc_getAssociatedObject(connection, CONNECTION_RECIEVED_DATA);
    NSURLResponse *response = (NSURLResponse *)objc_getAssociatedObject(connection, CONNECTION_HTTP_RESPONSE);
    ConnectionCompletionHandler block = objc_getAssociatedObject(connection, CONNECTION_COMPLETE_BLOCK);
    
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    NSString *contentType = [[httpResponse allHeaderFields] valueForKey:@"Content-Type"];
    
    NSNumber *expexted = objc_getAssociatedObject(connection, CONNECTION_EXPECTED_LENGTH);
    [self postConnectionNotifcation:connection expectedLength:expexted receivedLength:expexted];
    
    if ([contentType isEqualToString:@"application/json"]) {
        NSError *error = nil;
        id obj = [NSJSONSerialization JSONObjectWithData:recievedData options:NSJSONReadingMutableContainers error:&error];
        if (error) {
            [self connection:connection didFailWithError:error];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                block(obj, response, nil);
            });
        }
        
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            block(recievedData, response, nil);
        });
    }
    
    
    
    [self stopConnection:connection];
    
}


@end

