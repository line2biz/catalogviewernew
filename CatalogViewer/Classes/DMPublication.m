//
//  DMPublication.m
//  CatalogViewer
//
//  Created by Шурик on 13.03.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "DMPublication.h"
#import "DMCategory.h"


@implementation DMPublication

@dynamic activated;
@dynamic created;
@dynamic downloadAllowed;
@dynamic downloadURL;
@dynamic filesize;
@dynamic inLocalStore;
@dynamic localFolder;
@dynamic packageURL;
@dynamic previewImage;
@dynamic pubID;
@dynamic publicationStartLink;
@dynamic pubType;
@dynamic title;
@dynamic version;
@dynamic categories;
@dynamic unread;
@dynamic shared;

@end
