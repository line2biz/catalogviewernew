//
//  DMManager.h
//  MSDCatalog
//
//  Created by Александр Жовтый on 09.02.13.
//  Copyright (c) 2013 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>


FOUNDATION_EXPORT NSString *const DMManagerModelNameValue;
FOUNDATION_EXPORT NSString *const DMManagerErrorDomain;

#import "DMCategory+Category.h"
#import "DMPublication+Category.h"

@interface DMManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext * defaultContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel * managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator * persistentStoreCoordinator;

+ (DMManager *)sharedManager;
+ (NSManagedObjectContext *)managedObjectContext;
- (void)saveContext;
- (void)clearContext;

+ (BOOL)serializeDataDictionary:(NSDictionary *)dictionary error:(NSError **)error;

@end

@protocol DMManagerDisplayValueProtocol <NSObject>

@required
- (NSString *)displayNameForPropertyName:(NSString *)propertyName;
- (NSString *)displayValueForPropertyName:(NSString *)propertyName;

@end
