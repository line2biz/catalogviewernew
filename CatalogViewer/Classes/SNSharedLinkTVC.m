//
//  SNSharedLinkTVC.m
//  CatalogViewer
//
//  Created by Denis Harckevich on 04.07.17.
//  Copyright © 2017 Alexandr Zhovty. All rights reserved.
//

#import "SNSharedLinkTVC.h"

@interface SNSharedLinkTVC()
{
    __weak IBOutlet UIButton *_buttonDelete;
}

@end

@implementation SNSharedLinkTVC

#pragma mark - Life Cycle
- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    
    [_buttonDelete setImage:[[UIImage imageNamed:@"Icon-Trash"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    UIEdgeInsets insets = UIEdgeInsetsMake(10, 10, 10, 10);
    _buttonDelete.contentEdgeInsets = insets;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Private Methods
- (void)askDelegateToDeleteThis
{
    if (self.delegate != nil) {
        [self.delegate deleteCell:self];
    }
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonDelete:(id)sender
{
    [self askDelegateToDeleteThis];
}

@end


















