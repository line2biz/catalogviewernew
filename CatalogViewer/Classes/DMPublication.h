//
//  DMPublication.h
//  CatalogViewer
//
//  Created by Шурик on 13.03.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DMCategory, DMSharedPublications;

@interface DMPublication : NSManagedObject

@property (nonatomic, retain) NSNumber * activated;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSNumber * downloadAllowed;
@property (nonatomic, retain) NSString * downloadURL;
@property (nonatomic, retain) NSNumber * filesize;
@property (nonatomic, retain) NSNumber * inLocalStore;
@property (nonatomic, retain) NSString * localFolder;
@property (nonatomic, retain) NSString * packageURL;
@property (nonatomic, retain) NSString * previewImage;
@property (nonatomic, retain) NSNumber * pubID;
@property (nonatomic, retain) NSString * publicationStartLink;
@property (nonatomic, retain) NSNumber * pubType;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * version;
@property (nonatomic, retain) NSSet *categories;
@property (nonatomic, retain) NSNumber * unread;
@property (nonatomic, retain) DMSharedPublications *shared;
@end

@interface DMPublication (CoreDataGeneratedAccessors)

- (void)addCategoriesObject:(DMCategory *)value;
- (void)removeCategoriesObject:(DMCategory *)value;
- (void)addCategories:(NSSet *)values;
- (void)removeCategories:(NSSet *)values;

@end
